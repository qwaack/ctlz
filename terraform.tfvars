account_factory_product_id      = "prod-lx4nr7z6iybne"
account_factory_product_version = "pa-oyt2l22c7baxs"
accounts = [
  # {
  #   name                     = "test-account-00"
  #   provisioning_artifact_id = "pa-oyt2l22c7baxs"
  #   product_id               = "prod-lx4nr7z6iybne"
  #   provisioning_parameters = [
  #     {
  #       key   = "AccountName"
  #       value = "test00"
  #     },
  #     {
  #       key   = "ManagedOrganizationalUnit"
  #       value = "sandbox (ou-qexi-gt1mg4ag)"
  #     },
  #     {
  #       key   = "AccountEmail"
  #       value = "qwaack+test00@pm.me"
  #     },
  #     {
  #       key   = "SSOUserEmail"
  #       value = "qwaack+test00@pm.me"
  #     },
  #     {
  #       key   = "SSOUserFirstName"
  #       value = "Test00"
  #     },
  #     {
  #       key   = "SSOUserLastName"
  #       value = "AccountAdmin"
  #     }
  #   ]
  # },
  {
    name                     = "cicd0"
    provisioning_artifact_id = "pa-oyt2l22c7baxs"
    product_id               = "prod-lx4nr7z6iybne"
    provisioning_parameters = [
      {
        key   = "AccountName"
        value = "cicd0"
      },
      {
        key   = "ManagedOrganizationalUnit"
        value = "sandbox (ou-qexi-gt1mg4ag)"
      },
      {
        key   = "AccountEmail"
        value = "qwaack+cicd0@pm.me"
      },
      {
        key   = "SSOUserEmail"
        value = "qwaack+cicd0@pm.me"
      },
      {
        key   = "SSOUserFirstName"
        value = "cicd0"
      },
      {
        key   = "SSOUserLastName"
        value = "AccountAdmin"
      }
    ]
  },
  {
    name                     = "app0"
    provisioning_artifact_id = "pa-oyt2l22c7baxs"
    product_id               = "prod-lx4nr7z6iybne"
    provisioning_parameters = [
      {
        key   = "AccountName"
        value = "app0"
      },
      {
        key   = "ManagedOrganizationalUnit"
        value = "sandbox (ou-qexi-gt1mg4ag)"
      },
      {
        key   = "AccountEmail"
        value = "qwaack+app0@pm.me"
      },
      {
        key   = "SSOUserEmail"
        value = "qwaack+app0@pm.me"
      },
      {
        key   = "SSOUserFirstName"
        value = "app0"
      },
      {
        key   = "SSOUserLastName"
        value = "AccountAdmin"
      }
    ]
  },
  {
    name                     = "app0dev"
    provisioning_artifact_id = "pa-oyt2l22c7baxs"
    product_id               = "prod-lx4nr7z6iybne"
    provisioning_parameters = [
      {
        key   = "AccountName"
        value = "app0dev"
      },
      {
        key   = "ManagedOrganizationalUnit"
        value = "sandbox (ou-qexi-gt1mg4ag)"
      },
      {
        key   = "AccountEmail"
        value = "qwaack+app0dev@pm.me"
      },
      {
        key   = "SSOUserEmail"
        value = "qwaack+app0dev@pm.me"
      },
      {
        key   = "SSOUserFirstName"
        value = "app0dev"
      },
      {
        key   = "SSOUserLastName"
        value = "AccountAdmin"
      }
    ]
  },
  {
    name                     = "app0stage"
    provisioning_artifact_id = "pa-oyt2l22c7baxs"
    product_id               = "prod-lx4nr7z6iybne"
    provisioning_parameters = [
      {
        key   = "AccountName"
        value = "app0stage"
      },
      {
        key   = "ManagedOrganizationalUnit"
        value = "sandbox (ou-qexi-gt1mg4ag)"
      },
      {
        key   = "AccountEmail"
        value = "qwaack+app0stage@pm.me"
      },
      {
        key   = "SSOUserEmail"
        value = "qwaack+app0stage@pm.me"
      },
      {
        key   = "SSOUserFirstName"
        value = "app0stage"
      },
      {
        key   = "SSOUserLastName"
        value = "AccountAdmin"
      }
    ]
  },
  {
    name                     = "shared0"
    provisioning_artifact_id = "pa-oyt2l22c7baxs"
    product_id               = "prod-lx4nr7z6iybne"
    provisioning_parameters = [
      {
        key   = "AccountName"
        value = "shared0"
      },
      {
        key   = "ManagedOrganizationalUnit"
        value = "sandbox (ou-qexi-gt1mg4ag)"
      },
      {
        key   = "AccountEmail"
        value = "qwaack+shared0@pm.me"
      },
      {
        key   = "SSOUserEmail"
        value = "qwaack+shared0@pm.me"
      },
      {
        key   = "SSOUserFirstName"
        value = "shared0"
      },
      {
        key   = "SSOUserLastName"
        value = "AccountAdmin"
      }
    ]
  }
]
groups = [
  {
    name              = "app0admins"
    aws_account_names = ["cicd0", "app0", "qwaack", "test00", "app0dev", "app0stage"]
    users = [
      {
        email      = "qwaack+admin0@pm.me"
        first_name = "admin"
        last_name  = "0"
      },
      {
        email      = "qwaack+admin1@pm.me"
        first_name = "admin"
        last_name  = "1"
      },
      {
        email      = "qwaack+superuser1@pm.me"
        first_name = "superuser"
        last_name  = "1"
      },
    ]
  },
  {
    name              = "app0devs"
    aws_account_names = ["test00", "app0dev", "app0stage"]
    users = [
      {
        email      = "qwaack+dev0@pm.me"
        first_name = "dev"
        last_name  = "0"
      },
      {
        email      = "qwaack+dev1@pm.me"
        first_name = "dev"
        last_name  = "1"
      },
      {
        email      = "qwaack+superuser1@pm.me"
        first_name = "superuser"
        last_name  = "1"
      },
    ]
  }
]