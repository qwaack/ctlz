# AWS VPC Terraform Module

## Simple VPC

```hcl
module "vpc" {
  source              = "PATH/TO/MODULE"

  name                = "simple-vpc"
  cidr                = "10.0.0.0/16"

  azs                 = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets      = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway  = false
  single_nat_gateway  = true

  vpc_tags = {
    Name              = "vpc_name"
  }
}

```


## VPC with secondary CIDR blocks

```hcl
module "vpc" {
  source                = "PATH/TO/MODULE"

  name                  = "secondary-cidr-blocks-vpc"

  cidr                  = "10.0.0.0/16"
  secondary_cidr_blocks = ["10.1.0.0/16", "10.2.0.0/16"]

  azs                   = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets       = ["10.0.1.0/24", "10.1.2.0/24", "10.2.3.0/24"]
  public_subnets        = ["10.0.101.0/24", "10.1.102.0/24", "10.2.103.0/24"]

  enable_nat_gateway    = true
  single_nat_gateway    = true

  vpc_tags = {
    Name                = "vpc_name"
  }
}

```


## VPC with IPv6 enabled

```hcl
module "vpc" {
  source                                          = "PATH/TO/MODULE"

  name                                            = "ipv6-vpc"
  cidr                                            = "10.0.0.0/16"

  azs                                             = ["us-east-1a", "us-east-1b"]
  private_subnets                                 = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets                                  = ["10.0.101.0/24", "10.0.102.0/24"]
  intra_subnets                                   = ["10.0.103.0/24", "10.0.104.0/24"]

  enable_nat_gateway                              = false

  create_intra_subnet_route_table                 = true
  create_intra_internet_gateway_route             = true

  enable_ipv6                                     = true
  assign_ipv6_address_on_creation                 = true

  private_subnet_assign_ipv6_address_on_creation  = false

  public_subnet_ipv6_prefixes                     = [0, 1]
  private_subnet_ipv6_prefixes                    = [2, 3]
  intra_subnet_ipv6_prefixes                      = [4, 5]

  vpc_tags = {
    Name                                          = "vpc_name"
  }
}

```


## Network ACL

```hcl
locals {
  network_acls        = {
    default_inbound   = [
      {
        rule_number   = 900
        rule_action   = "allow"
        from_port     = 1024
        to_port       = 65535
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
    ]
    default_outbound  = [
      {
        rule_number   = 900
        rule_action   = "allow"
        from_port     = 32768
        to_port       = 65535
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
    ]
    public_inbound    = [
      {
        rule_number   = 100
        rule_action   = "allow"
        from_port     = 80
        to_port       = 80
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
      {
        rule_number   = 110
        rule_action   = "allow"
        from_port     = 443
        to_port       = 443
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
      {
        rule_number   = 120
        rule_action   = "allow"
        from_port     = 22
        to_port       = 22
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
    ]
    public_outbound   = [
      {
        rule_number   = 100
        rule_action   = "allow"
        from_port     = 80
        to_port       = 80
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
      {
        rule_number   = 110
        rule_action   = "allow"
        from_port     = 443
        to_port       = 443
        protocol      = "tcp"
        cidr_block    = "0.0.0.0/0"
      },
      {
        rule_number   = 120
        rule_action   = "allow"
        from_port     = 22
        to_port       = 22
        protocol      = "tcp"
        cidr_block    = "10.0.100.0/22"
      },
      {
        rule_number   = 130
        rule_action   = "allow"
        icmp_code     = -1
        icmp_type     = 8
        protocol      = "icmp"
        cidr_block    = "10.0.0.0/22"
      },
    ]
  }
}

module "vpc" {
  source                        = "PATH/TO/MODULE"

  name                          = "nacls-vpc"
  cidr                          = "10.0.0.0/16"

  azs                           = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets               = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets                = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  public_dedicated_network_acl  = true
  public_inbound_acl_rules      = concat(local.network_acls["default_inbound"], local.network_acls["public_inbound"])
  public_outbound_acl_rules     = concat(local.network_acls["default_outbound"], local.network_acls["public_outbound"])

  private_dedicated_network_acl = false

  manage_default_network_acl    = true

  enable_nat_gateway            = false
  single_nat_gateway            = true

  vpc_tags = {
    Name                        = "vpc_name"
  }
}

```
