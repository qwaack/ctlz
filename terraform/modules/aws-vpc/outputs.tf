############################################################
# Static values (arguments)
############################################################

output "azs" {
  value = var.azs
}

output "name" {
  value = var.name
}

output "tgw_id" {
  value = try(var.tgw_id, "")
}

############################################################
# VPC
############################################################

output "vpc_id" {
  value = try(aws_vpc.this[0].id, "")
}

output "vpc_arn" {
  value = try(aws_vpc.this[0].arn, "")
}

output "vpc_cidr_block" {
  value = try(aws_vpc.this[0].cidr_block, "")
}

output "default_security_group_id" {
  value = try(aws_vpc.this[0].default_security_group_id, "")
}

output "default_network_acl_id" {
  value = try(aws_vpc.this[0].default_network_acl_id, "")
}

output "default_route_table_id" {
  value = try(aws_vpc.this[0].default_route_table_id, "")
}

output "vpc_instance_tenancy" {
  value = try(aws_vpc.this[0].instance_tenancy, "")
}

output "vpc_enable_dns_support" {
  value = try(aws_vpc.this[0].enable_dns_support, "")
}

output "vpc_enable_dns_hostnames" {
  value = try(aws_vpc.this[0].enable_dns_hostnames, "")
}

output "vpc_main_route_table_id" {
  value = try(aws_vpc.this[0].main_route_table_id, "")
}

output "vpc_ipv6_association_id" {
  value = try(aws_vpc.this[0].ipv6_association_id, "")
}

output "vpc_ipv6_cidr_block" {
  value = try(aws_vpc.this[0].ipv6_cidr_block, "")
}

output "vpc_secondary_cidrs" {
  value = aws_vpc_ipv4_cidr_block_association.this[*].cidr_block
}

output "vpc_owner_id" {
  value = try(aws_vpc.this[0].owner_id, "")
}

output "private_subnets" {
  value = aws_subnet.private[*].id
}

output "private_subnet_arns" {
  value = aws_subnet.private[*].arn
}

output "private_subnet_azs" {
  value = aws_subnet.private[*].availability_zone
}

output "private_subnets_cidr_blocks" {
  value = aws_subnet.private[*].cidr_block
}

output "private_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.private[*].ipv6_cidr_block
}

output "database_subnets" {
  value = aws_subnet.database[*].id
}

output "database_subnet_arns" {
  value = aws_subnet.database[*].arn
}

output "database_subnet_azs" {
  value = aws_subnet.database[*].availability_zone
}

output "database_subnets_cidr_blocks" {
  value = aws_subnet.database[*].cidr_block
}

output "database_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.database[*].ipv6_cidr_block
}

output "public_subnets" {
  value = aws_subnet.public[*].id
}

output "public_subnet_arns" {
  value = aws_subnet.public[*].arn
}

output "public_subnet_azs" {
  value = aws_subnet.public[*].availability_zone
}

output "public_subnets_cidr_blocks" {
  value = aws_subnet.public[*].cidr_block
}

output "public_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.public[*].ipv6_cidr_block
}

output "intra_subnets" {
  value = aws_subnet.intra[*].id
}

output "intra_subnet_arns" {
  value = aws_subnet.intra[*].arn
}

output "intra_subnet_azs" {
  value = aws_subnet.intra[*].availability_zone
}

output "intra_subnets_cidr_blocks" {
  value = aws_subnet.intra[*].cidr_block
}

output "intra_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.intra[*].ipv6_cidr_block
}

output "vpn_subnets" {
  value = aws_subnet.vpn[*].id
}

output "vpn_subnet_arns" {
  value = aws_subnet.vpn[*].arn
}

output "vpn_subnet_azs" {
  value = aws_subnet.vpn[*].availability_zone
}

output "vpn_subnets_cidr_blocks" {
  value = aws_subnet.vpn[*].cidr_block
}

output "vpn_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.vpn[*].ipv6_cidr_block
}

output "customer_subnets" {
  value = aws_subnet.customer[*].id
}

output "customer_subnet_arns" {
  value = aws_subnet.customer[*].arn
}

output "customer_subnet_azs" {
  value = aws_subnet.customer[*].availability_zone
}

output "customer_subnets_cidr_blocks" {
  value = aws_subnet.customer[*].cidr_block
}

output "customer_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.customer[*].ipv6_cidr_block
}

output "tgw_subnets" {
  value = aws_subnet.tgw[*].id
}

output "tgw_subnet_arns" {
  value = aws_subnet.tgw[*].arn
}

output "tgw_subnet_azs" {
  value = aws_subnet.tgw[*].availability_zone
}

output "tgw_subnets_cidr_blocks" {
  value = aws_subnet.tgw[*].cidr_block
}

output "tgw_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.tgw[*].ipv6_cidr_block
}

output "firewall_subnets" {
  value = aws_subnet.firewall[*].id
}

output "firewall_subnet_arns" {
  value = aws_subnet.firewall[*].arn
}

output "firewall_subnet_azs" {
  value = aws_subnet.firewall[*].availability_zone
}

output "firewall_subnets_cidr_blocks" {
  value = aws_subnet.firewall[*].cidr_block
}

output "firewall_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.firewall[*].ipv6_cidr_block
}

output "endpoint_subnets" {
  value = aws_subnet.endpoint[*].id
}

output "endpoint_subnet_arns" {
  value = aws_subnet.endpoint[*].arn
}

output "endpoint_subnet_azs" {
  value = aws_subnet.endpoint[*].availability_zone
}

output "endpoint_subnets_cidr_blocks" {
  value = aws_subnet.endpoint[*].cidr_block
}

output "endpoint_subnets_ipv6_cidr_blocks" {
  value = aws_subnet.endpoint[*].ipv6_cidr_block
}

output "public_route_table_ids" {
  value = aws_route_table.public[*].id
}

output "private_route_table_ids" {
  value = aws_route_table.private[*].id
}

output "database_route_table_ids" {
  value = aws_route_table.database[*].id
}

output "intra_route_table_ids" {
  value = aws_route_table.intra[*].id
}

output "vpn_route_table_ids" {
  value = aws_route_table.vpn[*].id
}

output "customer_route_table_ids" {
  value = aws_route_table.customer[*].id
}

output "tgw_route_table_ids" {
  value = aws_route_table.tgw[*].id
}

output "firewall_route_table_ids" {
  value = aws_route_table.firewall[*].id
}

output "endpoint_route_table_ids" {
  value = aws_route_table.endpoint[*].id
}

output "public_internet_gateway_route_id" {
  value = try(aws_route.public_internet_gateway[0].id, "")
}

output "public_internet_gateway_ipv6_route_id" {
  value = try(aws_route.public_internet_gateway_ipv6[0].id, "")
}

output "private_nat_gateway_route_ids" {
  value = aws_route.private_nat_gateway[*].id
}

output "private_ipv6_egress_route_ids" {
  value = aws_route.private_ipv6_egress[*].id
}

output "private_route_table_association_ids" {
  value = aws_route_table_association.private[*].id
}

output "database_nat_gateway_route_ids" {
  value = aws_route.database_nat_gateway[*].id
}

output "database_ipv6_egress_route_ids" {
  value = aws_route.database_ipv6_egress[*].id
}

output "database_route_table_association_ids" {
  value = aws_route_table_association.database[*].id
}

output "intra_route_table_association_ids" {
  value = aws_route_table_association.intra[*].id
}

output "vpn_route_table_association_ids" {
  value = aws_route_table_association.vpn[*].id
}

output "customer_route_table_association_ids" {
  value = aws_route_table_association.customer[*].id
}

output "tgw_route_table_association_ids" {
  value = aws_route_table_association.tgw[*].id
}

output "firewall_route_table_association_ids" {
  value = aws_route_table_association.firewall[*].id
}

output "endpoint_route_table_association_ids" {
  value = aws_route_table_association.endpoint[*].id
}

output "public_route_table_association_ids" {
  value = aws_route_table_association.public[*].id
}

output "dhcp_options_id" {
  value = try(aws_vpc_dhcp_options.this[0].id, "")
}

output "nat_ids" {
  value = aws_eip.nat[*].id
}

output "nat_private_ips" {
  value = try(aws_nat_gateway.this[*].private_ip, [])
}

output "nat_public_ips" {
  value = try(aws_nat_gateway.this[*].public_ip, [])
}

output "natgw_ids" {
  value = aws_nat_gateway.this[*].id
}

output "customer_natgw_ids" {
  value = aws_nat_gateway.customer[*].id
}

output "igw_id" {
  value = try(aws_internet_gateway.this[0].id, "")
}

output "igw_arn" {
  value = try(aws_internet_gateway.this[0].arn, "")
}

output "egress_only_internet_gateway_id" {
  value = try(aws_egress_only_internet_gateway.this[0].id, "")
}

output "default_vpc_id" {
  value = try(aws_default_vpc.this[0].id, "")
}

output "default_vpc_arn" {
  value = try(aws_default_vpc.this[0].arn, "")
}

output "default_vpc_cidr_block" {
  value = try(aws_default_vpc.this[0].cidr_block, "")
}

output "default_vpc_default_security_group_id" {
  value = try(aws_default_vpc.this[0].default_security_group_id, "")
}

output "default_vpc_default_network_acl_id" {
  value = try(aws_default_vpc.this[0].default_network_acl_id, "")
}

output "default_vpc_default_route_table_id" {
  value = try(aws_default_vpc.this[0].default_route_table_id, "")
}

output "default_vpc_instance_tenancy" {
  value = try(aws_default_vpc.this[0].instance_tenancy, "")
}

output "default_vpc_enable_dns_support" {
  value = try(aws_default_vpc.this[0].enable_dns_support, "")
}

output "default_vpc_enable_dns_hostnames" {
  value = try(aws_default_vpc.this[0].enable_dns_hostnames, "")
}

output "default_vpc_main_route_table_id" {
  value = try(aws_default_vpc.this[0].main_route_table_id, "")
}

output "public_network_acl_id" {
  value = try(aws_network_acl.public[0].id, "")
}

output "public_network_acl_arn" {
  value = try(aws_network_acl.public[0].arn, "")
}

output "private_network_acl_id" {
  value = try(aws_network_acl.private[0].id, "")
}

output "private_network_acl_arn" {
  value = try(aws_network_acl.private[0].arn, "")
}

output "database_network_acl_id" {
  value = try(aws_network_acl.database[0].id, "")
}

output "database_network_acl_arn" {
  value = try(aws_network_acl.database[0].arn, "")
}

output "intra_network_acl_id" {
  value = try(aws_network_acl.intra[0].id, "")
}

output "intra_network_acl_arn" {
  value = try(aws_network_acl.intra[0].arn, "")
}

output "vpn_network_acl_id" {
  value = try(aws_network_acl.vpn[0].id, "")
}

output "vpn_network_acl_arn" {
  value = try(aws_network_acl.vpn[0].arn, "")
}

output "customer_network_acl_id" {
  value = try(aws_network_acl.customer[0].id, "")
}

output "customer_network_acl_arn" {
  value = try(aws_network_acl.customer[0].arn, "")
}

output "tgw_network_acl_id" {
  value = try(aws_network_acl.tgw[0].id, "")
}

output "tgw_network_acl_arn" {
  value = try(aws_network_acl.tgw[0].arn, "")
}

output "firewall_network_acl_id" {
  value = try(aws_network_acl.firewall[0].id, "")
}

output "firewall_network_acl_arn" {
  value = try(aws_network_acl.firewall[0].arn, "")
}

output "endpoint_network_acl_id" {
  value = try(aws_network_acl.endpoint[0].id, "")
}

output "endpoint_network_acl_arn" {
  value = try(aws_network_acl.endpoint[0].arn, "")
}

output "vpc_tags" {
  value = try(aws_vpc.this[0].tags, {})
}

output "default_rt_tags" {
  value = try(aws_default_route_table.default[0].tags, {})
}

output "default_nacl_tags" {
  value = try(aws_default_network_acl.this[0].tags, {})
}

output "default_sg_tags" {
  value = try(aws_default_security_group.this[0].tags, {})
}

output "igw_tags" {
  value = try(aws_internet_gateway.this[0].tags, {})
}

output "egress_only_igw_tags" {
  value = try(aws_egress_only_internet_gateway.this[0].tags, {})
}

output "nat_gateway_tags" {
  value = try(aws_nat_gateway.this[*].tags, {})
}

output "public_subnet_tags" {
  value = try(aws_subnet.public[*].tags, {})
}

output "private_subnet_tags" {
  value = try(aws_subnet.private[*].tags, {})
}

output "database_subnet_tags" {
  value = try(aws_subnet.database[*].tags, {})
}

output "intra_subnet_tags" {
  value = try(aws_subnet.intra[*].tags, {})
}

output "vpn_subnet_tags" {
  value = try(aws_subnet.vpn[*].tags, {})
}

output "customer_subnet_tags" {
  value = try(aws_subnet.customer[*].tags, {})
}

output "tgw_subnet_tags" {
  value = try(aws_subnet.tgw[*].tags, {})
}

output "firewall_subnet_tags" {
  value = try(aws_subnet.firewall[*].tags, {})
}

output "endpoint_subnet_tags" {
  value = try(aws_subnet.endpoint[*].tags, {})
}

output "public_rt_tags" {
  value = try(aws_route_table.public[*].tags, {})
}

output "private_rt_tags" {
  value = try(aws_route_table.private[*].tags, {})
}

output "database_rt_tags" {
  value = try(aws_route_table.database[*].tags, {})
}

output "intra_rt_tags" {
  value = try(aws_route_table.intra[*].tags, {})
}

output "vpn_rt_tags" {
  value = try(aws_route_table.vpn[*].tags, {})
}

output "customer_rt_tags" {
  value = try(aws_route_table.customer[*].tags, {})
}

output "tgw_rt_tags" {
  value = try(aws_route_table.tgw[*].tags, {})
}

output "firewall_rt_tags" {
  value = try(aws_route_table.firewall[*].tags, {})
}

output "endpoint_rt_tags" {
  value = try(aws_route_table.endpoint[*].tags, {})
}

output "public_nacl_tags" {
  value = try(aws_network_acl.public[*].tags, {})
}

output "private_nacl_tags" {
  value = try(aws_network_acl.private[*].tags, {})
}

output "database_nacl_tags" {
  value = try(aws_network_acl.database[*].tags, {})
}

output "intra_nacl_tags" {
  value = try(aws_network_acl.intra[*].tags, {})
}

output "vpn_nacl_tags" {
  value = try(aws_network_acl.vpn[*].tags, {})
}

output "customer_nacl_tags" {
  value = try(aws_network_acl.customer[*].tags, {})
}

output "tgw_nacl_tags" {
  value = try(aws_network_acl.tgw[*].tags, {})
}

output "firewall_nacl_tags" {
  value = try(aws_network_acl.firewall[*].tags, {})
}

output "endpoint_nacl_tags" {
  value = try(aws_network_acl.endpoint[*].tags, {})
}

############################################################
# VPC flow log
############################################################

output "vpc_flow_log_id" {
  value = try(aws_flow_log.this[0].id, "")
}

output "vpc_flow_log_destination_arn" {
  value = local.flow_log_destination_arn
}

output "vpc_flow_log_destination_type" {
  value = var.flow_log_destination_type
}

output "vpc_flow_log_cloudwatch_iam_role_arn" {
  value = local.flow_log_iam_role_arn
}

############################################################
# TGW
############################################################

output "tgw_attachment_id" {
  value = try(aws_ec2_transit_gateway_vpc_attachment.this[0].id, "")
}

output "tgw_attachment_tags" {
  value = try(aws_ec2_transit_gateway_vpc_attachment.this[0].tags, {})
}
