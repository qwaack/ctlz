############################################################
# AWS Region
############################################################

data "aws_region" "current" {}

############################################################
# Locals
############################################################

locals {
  max_subnet_length = max(
    length(var.private_subnets),
    length(var.database_subnets)
  )
  nat_gateway_count                    = var.single_nat_gateway ? 1 : var.one_nat_gateway_per_az ? length(var.azs) : local.max_subnet_length
  vpc_id                               = try(aws_vpc_ipv4_cidr_block_association.this[0].vpc_id, aws_vpc.this[0].id, "")
  enable_flow_log                      = var.create_vpc && var.enable_flow_log
  create_flow_log_cloudwatch_iam_role  = local.enable_flow_log && var.flow_log_destination_type != "s3" && var.create_flow_log_cloudwatch_iam_role
  create_flow_log_cloudwatch_log_group = local.enable_flow_log && var.flow_log_destination_type != "s3" && var.create_flow_log_cloudwatch_log_group
  flow_log_destination_arn             = local.create_flow_log_cloudwatch_log_group ? aws_cloudwatch_log_group.flow_log[0].arn : var.flow_log_destination_arn
  flow_log_iam_role_arn                = var.flow_log_destination_type != "s3" && local.create_flow_log_cloudwatch_iam_role ? aws_iam_role.vpc_flow_log_cloudwatch[0].arn : var.flow_log_cloudwatch_iam_role_arn
}

############################################################
# VPC
############################################################

resource "aws_vpc" "this" {
  count = var.create_vpc ? 1 : 0

  cidr_block                       = var.cidr
  instance_tenancy                 = var.instance_tenancy
  enable_dns_hostnames             = var.enable_dns_hostnames
  enable_dns_support               = var.enable_dns_support
  assign_generated_ipv6_cidr_block = var.enable_ipv6

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.vpc_tags,
  )
}

resource "aws_vpc_ipv4_cidr_block_association" "this" {
  count = var.create_vpc && length(var.secondary_cidrs) > 0 ? length(var.secondary_cidrs) : 0

  vpc_id = aws_vpc.this[0].id

  cidr_block = element(var.secondary_cidrs, count.index)
}

resource "aws_default_security_group" "this" {
  count = var.create_vpc && var.manage_default_security_group ? 1 : 0

  vpc_id = aws_vpc.this[0].id

  dynamic "ingress" {
    for_each = var.default_security_group_ingress
    content {
      self             = lookup(ingress.value, "self", null)
      cidr_blocks      = compact(split(",", lookup(ingress.value, "cidr_blocks", "")))
      ipv6_cidr_blocks = compact(split(",", lookup(ingress.value, "ipv6_cidr_blocks", "")))
      prefix_list_ids  = compact(split(",", lookup(ingress.value, "prefix_list_ids", "")))
      security_groups  = compact(split(",", lookup(ingress.value, "security_groups", "")))
      description      = lookup(ingress.value, "description", null)
      from_port        = lookup(ingress.value, "from_port", 0)
      to_port          = lookup(ingress.value, "to_port", 0)
      protocol         = lookup(ingress.value, "protocol", "-1")
    }
  }

  dynamic "egress" {
    for_each = var.default_security_group_egress
    content {
      self             = lookup(egress.value, "self", null)
      cidr_blocks      = compact(split(",", lookup(egress.value, "cidr_blocks", "")))
      ipv6_cidr_blocks = compact(split(",", lookup(egress.value, "ipv6_cidr_blocks", "")))
      prefix_list_ids  = compact(split(",", lookup(egress.value, "prefix_list_ids", "")))
      security_groups  = compact(split(",", lookup(egress.value, "security_groups", "")))
      description      = lookup(egress.value, "description", null)
      from_port        = lookup(egress.value, "from_port", 0)
      to_port          = lookup(egress.value, "to_port", 0)
      protocol         = lookup(egress.value, "protocol", "-1")
    }
  }

  tags = merge(
    { "Name" = "${var.name}-default" },
    var.tags,
    var.default_security_group_tags,
  )
}

############################################################
# DHCP Options Set
############################################################

resource "aws_vpc_dhcp_options" "this" {
  count = var.create_vpc && var.enable_dhcp_options ? 1 : 0

  domain_name          = var.dhcp_options_domain_name
  domain_name_servers  = var.dhcp_options_domain_name_servers
  ntp_servers          = var.dhcp_options_ntp_servers
  netbios_name_servers = var.dhcp_options_netbios_name_servers
  netbios_node_type    = var.dhcp_options_netbios_node_type

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.dhcp_options_tags,
  )
}

resource "aws_vpc_dhcp_options_association" "this" {
  count = var.create_vpc && var.enable_dhcp_options ? 1 : 0

  vpc_id          = local.vpc_id
  dhcp_options_id = aws_vpc_dhcp_options.this[0].id
}

############################################################
# Internet Gateway
############################################################

resource "aws_internet_gateway" "this" {
  count = var.create_vpc && var.create_igw && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.igw_tags,
  )
}

resource "aws_egress_only_internet_gateway" "this" {
  count = var.create_vpc && var.create_egress_only_igw && var.enable_ipv6 && local.max_subnet_length > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = var.name },
    var.tags,
    var.igw_tags,
  )
}

############################################################
# Internet Gateway route
############################################################

resource "aws_route_table" "igw" {
  count = var.create_vpc && var.create_igw && var.enable_firewall_routing ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-igw" },
    var.tags,
    var.igw_tags,
  )
}

resource "aws_route" "igw_firewall" {
  count = var.create_vpc && var.create_igw && var.enable_firewall_routing && length(var.firewall_endpoint_ids) > 0 && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0

  route_table_id         = aws_route_table.igw[0].id
  destination_cidr_block = var.public_subnets[count.index]
  vpc_endpoint_id        = var.firewall_endpoint_ids[count.index]

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "igw_firewall_ipv6" {
  count = var.create_vpc && var.create_igw && var.enable_ipv6 && var.enable_firewall_routing && length(var.firewall_endpoint_ids) > 0 && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0

  route_table_id              = aws_route_table.igw[0].id
  destination_ipv6_cidr_block = cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.public_subnet_ipv6_prefixes[count.index])
  vpc_endpoint_id             = var.firewall_endpoint_ids[count.index]
}

############################################################
# Default route
############################################################

resource "aws_default_route_table" "default" {
  count = var.create_vpc && var.manage_default_route_table ? 1 : 0

  default_route_table_id = aws_vpc.this[0].default_route_table_id
  propagating_vgws       = var.default_route_table_propagating_vgws

  dynamic "route" {
    for_each = var.default_route_table_routes
    content {
      cidr_block      = route.value.cidr_block
      ipv6_cidr_block = lookup(route.value, "ipv6_cidr_block", null)

      egress_only_gateway_id    = lookup(route.value, "egress_only_gateway_id", null)
      gateway_id                = lookup(route.value, "gateway_id", null)
      instance_id               = lookup(route.value, "instance_id", null)
      nat_gateway_id            = lookup(route.value, "nat_gateway_id", null)
      network_interface_id      = lookup(route.value, "network_interface_id", null)
      transit_gateway_id        = lookup(route.value, "transit_gateway_id", null)
      vpc_endpoint_id           = lookup(route.value, "vpc_endpoint_id", null)
      vpc_peering_connection_id = lookup(route.value, "vpc_peering_connection_id", null)
    }
  }

  timeouts {
    create = "5m"
    update = "5m"
  }

  tags = merge(
    { "Name" = "${var.name}-default" },
    var.tags,
    var.default_route_table_tags,
  )
}

############################################################
# Publiс routes
############################################################

resource "aws_route_table" "public" {
  count = var.create_vpc && !var.enable_firewall_routing && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.public_subnet_suffix}" },
    var.tags,
    var.public_route_table_tags,
  )
}

resource "aws_route" "public_internet_gateway" {
  count = var.create_vpc && var.create_igw && !var.enable_firewall_routing && length(var.public_subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this[0].id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "public_internet_gateway_ipv6" {
  count = var.create_vpc && var.create_igw && !var.enable_firewall_routing && var.enable_ipv6 && length(var.public_subnets) > 0 ? 1 : 0

  route_table_id              = aws_route_table.public[0].id
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.this[0].id
}

resource "aws_route_table" "public_fw" {
  count = var.create_vpc && var.enable_firewall_routing && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0

  vpc_id = local.vpc_id

  tags = merge(
    {
      "Name" = format("${var.name}-${var.public_subnet_suffix}-%s", element(var.azs, count.index))
    },
    var.tags,
    var.public_route_table_tags,
  )
}

resource "aws_route" "public_firewall" {
  count = var.create_vpc && var.enable_firewall_routing && length(var.firewall_endpoint_ids) > 0 && length(aws_route_table.public_fw) > 0 ? length(aws_route_table.public_fw) : 0

  route_table_id         = element(aws_route_table.public_fw[*].id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  vpc_endpoint_id        = var.firewall_endpoint_ids[count.index]

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "public_firewall_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.enable_firewall_routing && length(var.firewall_endpoint_ids) > 0 && length(aws_route_table.public_fw) > 0 ? length(aws_route_table.public_fw) : 0

  route_table_id              = element(aws_route_table.public_fw[*].id, count.index)
  destination_ipv6_cidr_block = "::/0"
  vpc_endpoint_id             = var.firewall_endpoint_ids[count.index]
}

############################################################
# Private routes
############################################################

resource "aws_route_table" "private" {
  count = var.create_vpc && local.max_subnet_length > 0 ? local.nat_gateway_count : 0

  vpc_id = local.vpc_id

  tags = merge(
    {
      "Name" = var.single_nat_gateway ? "${var.name}-${var.private_subnet_suffix}" : format(
        "${var.name}-${var.private_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.private_route_table_tags,
  )
}

resource "aws_route" "private_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.private_subnets) > 0 && length(var.private_tgw_routes) > 0 ? length(flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  transit_gateway_id = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private_tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.private_subnets) > 0 && length(var.private_tgw_routes_ipv6) > 0 ? length(flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.private_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  transit_gateway_id = var.tgw_id

  timeouts {
    create = "5m"
  }
}

############################################################
# Database routes
############################################################

resource "aws_route_table" "database" {
  count = var.create_vpc && length(var.database_subnets) > 0 && local.max_subnet_length > 0 ? local.nat_gateway_count : 0

  vpc_id = local.vpc_id

  tags = merge(
    {
      "Name" = var.single_nat_gateway ? "${var.name}-${var.database_subnet_suffix}" : format(
        "${var.name}-${var.database_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.database_route_table_tags,
  )
}

resource "aws_route" "database_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.database_subnets) > 0 && length(var.database_tgw_routes) > 0 ? length(flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  transit_gateway_id = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "database_tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.database_subnets) > 0 && length(var.database_tgw_routes_ipv6) > 0 ? length(flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.database_tgw_routes_ipv6 : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  transit_gateway_id = var.tgw_id

  timeouts {
    create = "5m"
  }
}

############################################################
# Intra routes
############################################################

resource "aws_route_table" "intra" {
  count = var.create_vpc && length(var.intra_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.intra_subnet_suffix}" },
    var.tags,
    var.intra_route_table_tags,
  )
}

resource "aws_route" "intra_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.intra_subnets) > 0 && length(var.intra_tgw_routes) > 0 ? length(var.intra_tgw_routes) : 0

  route_table_id         = aws_route_table.intra[0].id
  destination_cidr_block = var.intra_tgw_routes[count.index]
  transit_gateway_id     = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "intra_tgw_ipv6" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && var.enable_ipv6 && length(var.intra_subnets) > 0 && length(var.intra_tgw_routes_ipv6) > 0 ? length(var.intra_tgw_routes_ipv6) : 0

  route_table_id              = aws_route_table.intra[0].id
  destination_ipv6_cidr_block = var.intra_tgw_routes_ipv6[count.index]
  transit_gateway_id          = var.tgw_id
}

############################################################
# Customer routes
############################################################

resource "aws_route_table" "customer" {
  count = var.create_vpc && length(var.customer_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.customer_subnet_suffix}" },
    var.tags,
    var.customer_route_table_tags,
  )
}

resource "aws_route" "customer_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.customer_subnets) > 0 && length(var.customer_tgw_routes) > 0 ? length(var.customer_tgw_routes) : 0

  route_table_id         = aws_route_table.customer[0].id
  destination_cidr_block = var.customer_tgw_routes[count.index]
  transit_gateway_id     = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "customer_tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.customer_subnets) > 0 && length(var.customer_tgw_routes_ipv6) > 0 ? length(var.customer_tgw_routes_ipv6) : 0

  route_table_id              = aws_route_table.customer[0].id
  destination_ipv6_cidr_block = var.customer_tgw_routes_ipv6[count.index]
  transit_gateway_id          = var.tgw_id
}

############################################################
# VPN routes
############################################################

resource "aws_route_table" "vpn" {
  count = var.create_vpc && length(var.vpn_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.vpn_subnet_suffix}" },
    var.tags,
    var.vpn_route_table_tags,
  )
}

resource "aws_route" "vpn_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.vpn_subnets) > 0 && length(var.vpn_tgw_routes) > 0 ? length(var.vpn_tgw_routes) : 0

  route_table_id         = aws_route_table.vpn[0].id
  destination_cidr_block = var.vpn_tgw_routes[count.index]
  transit_gateway_id     = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "vpn_tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.vpn_subnets) > 0 && length(var.vpn_tgw_routes_ipv6) > 0 ? length(var.vpn_tgw_routes_ipv6) : 0

  route_table_id              = aws_route_table.vpn[0].id
  destination_ipv6_cidr_block = var.vpn_tgw_routes_ipv6[count.index]
  transit_gateway_id          = var.tgw_id
}

############################################################
# TGW routes
############################################################

resource "aws_route_table" "tgw" {
  count = var.create_vpc && length(var.tgw_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.tgw_subnet_suffix}" },
    var.tags,
    var.tgw_route_table_tags,
  )
}

resource "aws_route" "tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.tgw_subnets) > 0 && length(var.tgw_routes) > 0 ? length(var.tgw_routes) : 0

  route_table_id         = aws_route_table.tgw[0].id
  destination_cidr_block = var.tgw_routes[count.index]
  transit_gateway_id     = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.tgw_subnets) > 0 && length(var.tgw_routes_ipv6) > 0 ? length(var.tgw_routes_ipv6) : 0

  route_table_id              = aws_route_table.tgw[0].id
  destination_ipv6_cidr_block = var.tgw_routes_ipv6[count.index]
  transit_gateway_id          = var.tgw_id
}

############################################################
# Firewall routes
############################################################

resource "aws_route_table" "firewall" {
  count = var.create_vpc && length(var.firewall_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.firewall_subnet_suffix}" },
    var.tags,
    var.firewall_route_table_tags,
  )
}

resource "aws_route" "firewall_internet_gateway" {
  count = var.create_vpc && var.create_igw && var.enable_firewall_routing && length(var.firewall_subnets) > 0 ? 1 : 0

  route_table_id         = aws_route_table.firewall[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this[0].id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "firewall_internet_gateway_ipv6" {
  count = var.create_vpc && var.create_igw && var.enable_firewall_routing && var.enable_ipv6 && length(var.firewall_subnets) > 0 ? 1 : 0

  route_table_id              = aws_route_table.firewall[0].id
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.this[0].id
}

resource "aws_route" "firewall_tgw" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.firewall_subnets) > 0 && length(var.firewall_tgw_routes) > 0 ? length(var.firewall_tgw_routes) : 0

  route_table_id         = aws_route_table.firewall[0].id
  destination_cidr_block = var.firewall_tgw_routes[count.index]
  transit_gateway_id     = var.tgw_id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "firewall_tgw_ipv6" {
  count = var.create_vpc && var.enable_ipv6 && var.tgw_create_attachment && length(var.tgw_id) > 0 && length(var.firewall_subnets) > 0 && length(var.firewall_tgw_routes_ipv6) > 0 ? length(var.firewall_tgw_routes_ipv6) : 0

  route_table_id              = aws_route_table.firewall[0].id
  destination_ipv6_cidr_block = var.firewall_tgw_routes_ipv6[count.index]
  transit_gateway_id          = var.tgw_id
}

############################################################
# Endpoint subnet routes
############################################################

resource "aws_route_table" "endpoint" {
  count = var.create_vpc && length(var.endpoint_subnets) > 0 ? 1 : 0

  vpc_id = local.vpc_id

  tags = merge(
    { "Name" = "${var.name}-${var.endpoint_subnet_suffix}" },
    var.tags,
    var.endpoint_route_table_tags,
  )
}

############################################################
# Public subnet
############################################################

resource "aws_subnet" "public" {
  count = var.create_vpc && length(var.public_subnets) > 0 && (false == var.one_nat_gateway_per_az || length(var.public_subnets) >= length(var.azs)) ? length(var.public_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = element(concat(var.public_subnets, [""]), count.index)
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  map_public_ip_on_launch         = var.map_public_ip_on_launch
  assign_ipv6_address_on_creation = var.public_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.public_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.public_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.public_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.public_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.public_subnet_tags,
  )
}

############################################################
# Private subnet
############################################################

resource "aws_subnet" "private" {
  count = var.create_vpc && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.private_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.private_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.private_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.private_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.private_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.private_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.private_subnet_tags,
  )
}

############################################################
# Database subnet
############################################################

resource "aws_subnet" "database" {
  count = var.create_vpc && length(var.database_subnets) > 0 ? length(var.database_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.database_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.database_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.database_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.database_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.database_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.database_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.database_subnet_tags,
  )
}

############################################################
# Intra subnets - private subnet without NAT gateway
############################################################

resource "aws_subnet" "intra" {
  count = var.create_vpc && length(var.intra_subnets) > 0 ? length(var.intra_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.intra_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.intra_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.intra_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.intra_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.intra_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.intra_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.intra_subnet_tags,
  )
}

############################################################
# Customer subnets
############################################################

resource "aws_subnet" "customer" {
  count = var.create_vpc && length(var.customer_subnets) > 0 ? length(var.customer_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.customer_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.customer_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.customer_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.customer_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.customer_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.customer_subnet_suffix}-${var.customer_subnets[count.index]}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.customer_subnet_tags,
  )
}

############################################################
# VPN subnets
############################################################

resource "aws_subnet" "vpn" {
  count = var.create_vpc && length(var.vpn_subnets) > 0 ? length(var.vpn_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.vpn_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.vpn_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.vpn_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.vpn_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.vpn_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.vpn_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.vpn_subnet_tags,
  )
}

############################################################
# TGW subnets
############################################################

resource "aws_subnet" "tgw" {
  count = var.create_vpc && length(var.tgw_subnets) > 0 ? length(var.tgw_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.tgw_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.tgw_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.tgw_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.tgw_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.tgw_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.tgw_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.tgw_subnet_tags,
  )
}

############################################################
# Firewall subnets
############################################################

resource "aws_subnet" "firewall" {
  count = var.create_vpc && length(var.firewall_subnets) > 0 ? length(var.firewall_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.firewall_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.firewall_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.firewall_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.firewall_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.firewall_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.firewall_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.firewall_subnet_tags,
  )
}

############################################################
# Endpoint subnets
############################################################

resource "aws_subnet" "endpoint" {
  count = var.create_vpc && length(var.endpoint_subnets) > 0 ? length(var.endpoint_subnets) : 0

  vpc_id                          = local.vpc_id
  cidr_block                      = var.endpoint_subnets[count.index]
  availability_zone               = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) > 0 ? element(var.azs, count.index) : null
  availability_zone_id            = length(regexall("^[a-z]{2}-", element(var.azs, count.index))) == 0 ? element(var.azs, count.index) : null
  assign_ipv6_address_on_creation = var.endpoint_subnet_assign_ipv6_address_on_creation == null ? var.assign_ipv6_address_on_creation : var.endpoint_subnet_assign_ipv6_address_on_creation

  ipv6_cidr_block = var.enable_ipv6 && length(var.endpoint_subnet_ipv6_prefixes) > 0 ? cidrsubnet(aws_vpc.this[0].ipv6_cidr_block, 8, var.endpoint_subnet_ipv6_prefixes[count.index]) : null

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.endpoint_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.endpoint_subnet_tags,
  )
}

############################################################
# Default Network ACLs
############################################################

resource "aws_default_network_acl" "this" {
  count = var.create_vpc && var.manage_default_network_acl ? 1 : 0

  default_network_acl_id = aws_vpc.this[0].default_network_acl_id

  subnet_ids = null

  dynamic "ingress" {
    for_each = var.default_network_acl_ingress
    content {
      action          = ingress.value.action
      cidr_block      = lookup(ingress.value, "cidr_block", null)
      from_port       = ingress.value.from_port
      icmp_code       = lookup(ingress.value, "icmp_code", null)
      icmp_type       = lookup(ingress.value, "icmp_type", null)
      ipv6_cidr_block = lookup(ingress.value, "ipv6_cidr_block", null)
      protocol        = ingress.value.protocol
      rule_no         = ingress.value.rule_no
      to_port         = ingress.value.to_port
    }
  }
  dynamic "egress" {
    for_each = var.default_network_acl_egress
    content {
      action          = egress.value.action
      cidr_block      = lookup(egress.value, "cidr_block", null)
      from_port       = egress.value.from_port
      icmp_code       = lookup(egress.value, "icmp_code", null)
      icmp_type       = lookup(egress.value, "icmp_type", null)
      ipv6_cidr_block = lookup(egress.value, "ipv6_cidr_block", null)
      protocol        = egress.value.protocol
      rule_no         = egress.value.rule_no
      to_port         = egress.value.to_port
    }
  }

  tags = merge(
    { "Name" = "${var.name}-default" },
    var.tags,
    var.default_network_acl_tags,
  )

  lifecycle {
    ignore_changes = [subnet_ids]
  }
}

############################################################
# Public Network ACLs
############################################################

resource "aws_network_acl" "public" {
  count = var.create_vpc && var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.public[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.public_subnet_suffix}" },
    var.tags,
    var.public_acl_tags,
  )
}

resource "aws_network_acl_rule" "public_inbound" {
  count = var.create_vpc && var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? length(var.public_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.public[0].id

  egress          = false
  rule_number     = var.public_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.public_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.public_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.public_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.public_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.public_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.public_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.public_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.public_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "public_outbound" {
  count = var.create_vpc && var.public_dedicated_network_acl && length(var.public_subnets) > 0 ? length(var.public_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.public[0].id

  egress          = true
  rule_number     = var.public_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.public_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.public_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.public_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.public_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.public_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.public_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.public_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.public_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Private Network ACLs
############################################################

resource "aws_network_acl" "private" {
  count = var.create_vpc && var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.private[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.private_subnet_suffix}" },
    var.tags,
    var.private_acl_tags,
  )
}

resource "aws_network_acl_rule" "private_inbound" {
  count = var.create_vpc && var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? length(var.private_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.private[0].id

  egress          = false
  rule_number     = var.private_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.private_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.private_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.private_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.private_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.private_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.private_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.private_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.private_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "private_outbound" {
  count = var.create_vpc && var.private_dedicated_network_acl && length(var.private_subnets) > 0 ? length(var.private_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.private[0].id

  egress          = true
  rule_number     = var.private_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.private_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.private_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.private_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.private_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.private_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.private_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.private_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.private_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Database Network ACLs
############################################################

resource "aws_network_acl" "database" {
  count = var.create_vpc && var.database_dedicated_network_acl && length(var.database_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.database[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.database_subnet_suffix}" },
    var.tags,
    var.database_acl_tags,
  )
}

resource "aws_network_acl_rule" "database_inbound" {
  count = var.create_vpc && var.database_dedicated_network_acl && length(var.database_subnets) > 0 ? length(var.database_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.database[0].id

  egress          = false
  rule_number     = var.database_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.database_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.database_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.database_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.database_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.database_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.database_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.database_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.database_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "database_outbound" {
  count = var.create_vpc && var.database_dedicated_network_acl && length(var.database_subnets) > 0 ? length(var.database_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.database[0].id

  egress          = true
  rule_number     = var.database_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.database_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.database_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.database_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.database_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.database_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.database_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.database_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.database_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Intra Network ACLs
############################################################

resource "aws_network_acl" "intra" {
  count = var.create_vpc && var.intra_dedicated_network_acl && length(var.intra_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.intra[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.intra_subnet_suffix}" },
    var.tags,
    var.intra_acl_tags,
  )
}

resource "aws_network_acl_rule" "intra_inbound" {
  count = var.create_vpc && var.intra_dedicated_network_acl && length(var.intra_subnets) > 0 ? length(var.intra_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.intra[0].id

  egress          = false
  rule_number     = var.intra_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.intra_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.intra_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.intra_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.intra_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.intra_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.intra_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.intra_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.intra_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "intra_outbound" {
  count = var.create_vpc && var.intra_dedicated_network_acl && length(var.intra_subnets) > 0 ? length(var.intra_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.intra[0].id

  egress          = true
  rule_number     = var.intra_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.intra_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.intra_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.intra_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.intra_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.intra_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.intra_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.intra_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.intra_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Customer Network ACLs
############################################################

resource "aws_network_acl" "customer" {
  count = var.create_vpc && var.customer_dedicated_network_acl && length(var.customer_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.customer[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.customer_subnet_suffix}" },
    var.tags,
    var.customer_acl_tags,
  )
}

resource "aws_network_acl_rule" "customer_inbound" {
  count = var.create_vpc && var.customer_dedicated_network_acl && length(var.customer_subnets) > 0 ? length(var.customer_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.customer[0].id

  egress          = false
  rule_number     = var.customer_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.customer_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.customer_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.customer_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.customer_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.customer_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.customer_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.customer_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.customer_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "customer_outbound" {
  count = var.create_vpc && var.customer_dedicated_network_acl && length(var.customer_subnets) > 0 ? length(var.customer_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.customer[0].id

  egress          = true
  rule_number     = var.customer_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.customer_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.customer_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.customer_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.customer_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.customer_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.customer_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.customer_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.customer_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# VPN Network ACLs
############################################################

resource "aws_network_acl" "vpn" {
  count = var.create_vpc && var.vpn_dedicated_network_acl && length(var.vpn_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.vpn[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.vpn_subnet_suffix}" },
    var.tags,
    var.vpn_acl_tags,
  )
}

resource "aws_network_acl_rule" "vpn_inbound" {
  count = var.create_vpc && var.vpn_dedicated_network_acl && length(var.vpn_subnets) > 0 ? length(var.vpn_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.vpn[0].id

  egress          = false
  rule_number     = var.vpn_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.vpn_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.vpn_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.vpn_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.vpn_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.vpn_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.vpn_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.vpn_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.vpn_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "vpn_outbound" {
  count = var.create_vpc && var.vpn_dedicated_network_acl && length(var.vpn_subnets) > 0 ? length(var.vpn_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.vpn[0].id

  egress          = true
  rule_number     = var.vpn_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.vpn_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.vpn_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.vpn_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.vpn_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.vpn_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.vpn_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.vpn_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.vpn_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# TGW Network ACLs
############################################################

resource "aws_network_acl" "tgw" {
  count = var.create_vpc && var.tgw_dedicated_network_acl && length(var.tgw_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.tgw[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.tgw_subnet_suffix}" },
    var.tags,
    var.tgw_acl_tags,
  )
}

resource "aws_network_acl_rule" "tgw_inbound" {
  count = var.create_vpc && var.tgw_dedicated_network_acl && length(var.tgw_subnets) > 0 ? length(var.tgw_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.tgw[0].id

  egress          = false
  rule_number     = var.tgw_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.tgw_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.tgw_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.tgw_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.tgw_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.tgw_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.tgw_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.tgw_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.tgw_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "tgw_outbound" {
  count = var.create_vpc && var.tgw_dedicated_network_acl && length(var.tgw_subnets) > 0 ? length(var.tgw_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.tgw[0].id

  egress          = true
  rule_number     = var.tgw_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.tgw_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.tgw_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.tgw_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.tgw_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.tgw_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.tgw_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.tgw_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.tgw_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Firewall Network ACLs
############################################################

resource "aws_network_acl" "firewall" {
  count = var.create_vpc && var.firewall_dedicated_network_acl && length(var.firewall_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.firewall[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.firewall_subnet_suffix}" },
    var.tags,
    var.firewall_acl_tags,
  )
}

resource "aws_network_acl_rule" "firewall_inbound" {
  count = var.create_vpc && var.firewall_dedicated_network_acl && length(var.firewall_subnets) > 0 ? length(var.firewall_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.firewall[0].id

  egress          = false
  rule_number     = var.firewall_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.firewall_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.firewall_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.firewall_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.firewall_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.firewall_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.firewall_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.firewall_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.firewall_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "firewall_outbound" {
  count = var.create_vpc && var.firewall_dedicated_network_acl && length(var.firewall_subnets) > 0 ? length(var.firewall_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.firewall[0].id

  egress          = true
  rule_number     = var.firewall_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.firewall_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.firewall_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.firewall_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.firewall_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.firewall_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.firewall_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.firewall_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.firewall_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# Endpoint Network ACLs
############################################################

resource "aws_network_acl" "endpoint" {
  count = var.create_vpc && var.endpoint_dedicated_network_acl && length(var.endpoint_subnets) > 0 ? 1 : 0

  vpc_id     = local.vpc_id
  subnet_ids = aws_subnet.endpoint[*].id

  tags = merge(
    { "Name" = "${var.name}-${var.endpoint_subnet_suffix}" },
    var.tags,
    var.endpoint_acl_tags,
  )
}

resource "aws_network_acl_rule" "endpoint_inbound" {
  count = var.create_vpc && var.endpoint_dedicated_network_acl && length(var.endpoint_subnets) > 0 ? length(var.endpoint_inbound_acl_rules) : 0

  network_acl_id = aws_network_acl.endpoint[0].id

  egress          = false
  rule_number     = var.endpoint_inbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.endpoint_inbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.endpoint_inbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.endpoint_inbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.endpoint_inbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.endpoint_inbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.endpoint_inbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.endpoint_inbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.endpoint_inbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "endpoint_outbound" {
  count = var.create_vpc && var.endpoint_dedicated_network_acl && length(var.endpoint_subnets) > 0 ? length(var.endpoint_outbound_acl_rules) : 0

  network_acl_id = aws_network_acl.endpoint[0].id

  egress          = true
  rule_number     = var.endpoint_outbound_acl_rules[count.index]["rule_number"]
  rule_action     = var.endpoint_outbound_acl_rules[count.index]["rule_action"]
  from_port       = lookup(var.endpoint_outbound_acl_rules[count.index], "from_port", null)
  to_port         = lookup(var.endpoint_outbound_acl_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.endpoint_outbound_acl_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.endpoint_outbound_acl_rules[count.index], "icmp_type", null)
  protocol        = var.endpoint_outbound_acl_rules[count.index]["protocol"]
  cidr_block      = lookup(var.endpoint_outbound_acl_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.endpoint_outbound_acl_rules[count.index], "ipv6_cidr_block", null)
}

############################################################
# NAT Gateway
############################################################

locals {
  nat_gateway_ips = var.reuse_nat_ips ? var.external_nat_ip_ids : try(aws_eip.nat[*].id, [])
}

resource "aws_eip" "nat" {
  count = var.create_vpc && var.enable_nat_gateway && false == var.reuse_nat_ips ? local.nat_gateway_count : 0

  vpc = true

  tags = merge(
    {
      "Name" = format(
        "${var.name}-%s",
        element(var.azs, var.single_nat_gateway ? 0 : count.index),
      )
    },
    var.tags,
    var.nat_eip_tags,
  )
}

resource "aws_nat_gateway" "this" {
  count = var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0

  allocation_id = element(
    local.nat_gateway_ips,
    var.single_nat_gateway ? 0 : count.index,
  )
  subnet_id = element(
    aws_subnet.public[*].id,
    var.single_nat_gateway ? 0 : count.index,
  )

  tags = merge(
    {
      "Name" = format(
        "${var.name}-%s",
        element(var.azs, var.single_nat_gateway ? 0 : count.index),
      )
    },
    var.tags,
    var.nat_gateway_tags,
  )

  depends_on = [aws_internet_gateway.this]
}

resource "aws_route" "private_nat_gateway" {
  count = var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0

  route_table_id         = element(aws_route_table.private[*].id, count.index)
  destination_cidr_block = var.nat_gateway_destination_cidr_block
  nat_gateway_id         = element(aws_nat_gateway.this[*].id, count.index)

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private_ipv6_egress" {
  count = var.create_vpc && var.create_egress_only_igw && var.enable_ipv6 ? length(var.private_subnets) : 0

  route_table_id              = element(aws_route_table.private[*].id, count.index)
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = element(aws_egress_only_internet_gateway.this[*].id, 0)
}

resource "aws_route" "database_nat_gateway" {
  count = var.create_vpc && var.enable_nat_gateway ? local.nat_gateway_count : 0

  route_table_id         = element(aws_route_table.database[*].id, count.index)
  destination_cidr_block = var.nat_gateway_destination_cidr_block
  nat_gateway_id         = element(aws_nat_gateway.this[*].id, count.index)

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "database_ipv6_egress" {
  count = var.create_vpc && var.create_egress_only_igw && var.enable_ipv6 ? length(var.database_subnets) : 0

  route_table_id              = element(aws_route_table.database[*].id, count.index)
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = element(aws_egress_only_internet_gateway.this[*].id, 0)
}

############################################################
# Private NAT Gateway
############################################################

resource "aws_nat_gateway" "customer" {
  count = var.create_vpc && var.enable_customer_nat && length(var.customer_subnets) > 0 ? length(var.customer_subnets) : 0

  connectivity_type = "private"
  subnet_id         = element(aws_subnet.customer[*].id, count.index)

  tags = merge(
    {
      "Name" = format(
        "${var.name}-${var.customer_subnet_suffix}-%s",
        element(var.azs, count.index),
      )
    },
    var.tags,
    var.customer_nat_gateway_tags,
  )
}

resource "aws_route" "private_customer_nat" {
  count = var.create_vpc && var.enable_customer_nat && length(var.customer_private_routes) > 0 ? length(flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.customer_private_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.customer_private_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.private[*].id : [
      for cidr in var.customer_private_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  nat_gateway_id = aws_nat_gateway.customer[count.index]

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "database_customer_nat" {
  count = var.create_vpc && var.enable_customer_nat && length(var.customer_database_routes) > 0 ? length(flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.customer_database_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])) : 0

  route_table_id = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.customer_database_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].route_table

  destination_cidr_block = flatten([
    for i, rt in aws_route_table.database[*].id : [
      for cidr in var.customer_database_routes : {
        route_table = rt
        cidr        = cidr
      }
    ]
  ])[count.index].cidr

  nat_gateway_id = aws_nat_gateway.customer[count.index]

  timeouts {
    create = "5m"
  }
}

############################################################
# Route table association
############################################################

resource "aws_route_table_association" "igw" {
  count = var.create_vpc && var.create_igw && var.enable_firewall_routing ? 1 : 0

  gateway_id     = aws_internet_gateway.this[0].id
  route_table_id = aws_route_table.igw[0].id
}

resource "aws_route_table_association" "private" {
  count = var.create_vpc && length(var.private_subnets) > 0 ? length(var.private_subnets) : 0

  subnet_id      = element(aws_subnet.private[*].id, count.index)
  route_table_id = element(aws_route_table.private[*].id, var.single_nat_gateway ? 0 : count.index)
}

resource "aws_route_table_association" "database" {
  count = var.create_vpc && length(var.database_subnets) > 0 ? length(var.database_subnets) : 0

  subnet_id      = element(aws_subnet.database[*].id, count.index)
  route_table_id = element(aws_route_table.database[*].id, var.single_nat_gateway ? 0 : count.index)
}

resource "aws_route_table_association" "public" {
  count = var.create_vpc && length(var.public_subnets) > 0 ? length(var.public_subnets) : 0

  subnet_id      = element(aws_subnet.public[*].id, count.index)
  route_table_id = var.enable_firewall_routing ? element(aws_route_table.public_fw[*].id, count.index) : element(aws_route_table.public[*].id, 0)
}

resource "aws_route_table_association" "intra" {
  count = var.create_vpc && length(var.intra_subnets) > 0 ? length(var.intra_subnets) : 0

  subnet_id      = element(aws_subnet.intra[*].id, count.index)
  route_table_id = element(aws_route_table.intra[*].id, 0)
}

resource "aws_route_table_association" "vpn" {
  count = var.create_vpc && length(var.vpn_subnets) > 0 ? length(var.vpn_subnets) : 0

  subnet_id      = element(aws_subnet.vpn[*].id, count.index)
  route_table_id = element(aws_route_table.vpn[*].id, 0)
}

resource "aws_route_table_association" "customer" {
  count = var.create_vpc && length(var.customer_subnets) > 0 ? length(var.customer_subnets) : 0

  subnet_id      = element(aws_subnet.customer[*].id, count.index)
  route_table_id = element(aws_route_table.customer[*].id, 0)
}

resource "aws_route_table_association" "tgw" {
  count = var.create_vpc && length(var.tgw_subnets) > 0 ? length(var.tgw_subnets) : 0

  subnet_id      = element(aws_subnet.tgw[*].id, count.index)
  route_table_id = element(aws_route_table.tgw[*].id, 0)
}

resource "aws_route_table_association" "firewall" {
  count = var.create_vpc && length(var.firewall_subnets) > 0 ? length(var.firewall_subnets) : 0

  subnet_id      = element(aws_subnet.firewall[*].id, count.index)
  route_table_id = element(aws_route_table.firewall[*].id, 0)
}

resource "aws_route_table_association" "endpoint" {
  count = var.create_vpc && length(var.endpoint_subnets) > 0 ? length(var.endpoint_subnets) : 0

  subnet_id      = element(aws_subnet.endpoint[*].id, count.index)
  route_table_id = element(aws_route_table.endpoint[*].id, 0)
}

############################################################
# Flow Log
############################################################

resource "aws_flow_log" "this" {
  count = local.enable_flow_log ? 1 : 0

  log_destination_type     = var.flow_log_destination_type
  log_destination          = local.flow_log_destination_arn
  log_format               = var.flow_log_log_format
  iam_role_arn             = local.flow_log_iam_role_arn
  traffic_type             = var.flow_log_traffic_type
  vpc_id                   = local.vpc_id
  max_aggregation_interval = var.flow_log_max_aggregation_interval

  dynamic "destination_options" {
    for_each = var.flow_log_destination_type == "s3" ? [true] : []

    content {
      file_format                = var.flow_log_file_format
      hive_compatible_partitions = var.flow_log_hive_compatible_partitions
      per_hour_partition         = var.flow_log_per_hour_partition
    }
  }

  tags = merge(var.tags, var.vpc_flow_log_tags)
}

############################################################
# Flow Log CloudWatch
############################################################

resource "aws_cloudwatch_log_group" "flow_log" {
  count = local.create_flow_log_cloudwatch_log_group ? 1 : 0

  name              = var.flow_log_cloudwatch_log_group_name
  retention_in_days = var.flow_log_cloudwatch_log_group_retention_in_days
  kms_key_id        = var.flow_log_cloudwatch_log_group_kms_key_id

  tags = merge(var.tags, var.vpc_flow_log_tags)
}

resource "aws_iam_role" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  name_prefix          = "vpc-flow-log-role-"
  assume_role_policy   = data.aws_iam_policy_document.flow_log_cloudwatch_assume_role[0].json
  permissions_boundary = var.vpc_flow_log_permissions_boundary

  tags = merge(var.tags, var.vpc_flow_log_tags)
}

data "aws_iam_policy_document" "flow_log_cloudwatch_assume_role" {
  count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  statement {
    sid = "AWSVPCFlowLogsAssumeRole"

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }

    effect = "Allow"

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role_policy_attachment" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  role       = aws_iam_role.vpc_flow_log_cloudwatch[0].name
  policy_arn = aws_iam_policy.vpc_flow_log_cloudwatch[0].arn
}

resource "aws_iam_policy" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  name_prefix = "vpc-flow-log-to-cloudwatch-"
  policy      = data.aws_iam_policy_document.vpc_flow_log_cloudwatch[0].json
  tags        = merge(var.tags, var.vpc_flow_log_tags)
}

data "aws_iam_policy_document" "vpc_flow_log_cloudwatch" {
  count = local.create_flow_log_cloudwatch_iam_role ? 1 : 0

  statement {
    sid = "AWSVPCFlowLogsPushToCloudWatch"

    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
    ]

    resources = ["*"]
  }
}

############################################################
# Flow Log CloudWatch Dashboard
############################################################

locals {
  flow_log_cloudwatch_log_group_name = local.enable_flow_log ? aws_cloudwatch_log_group.flow_log[0].name : ""
  flow_log_dashboard_widgets = [
    {
      "type" : "log",
      "x" : 0,
      "y" : 0,
      "width" : 15,
      "height" : 10,
      "properties" : {
        "region" : data.aws_region.current.name,
        "title" : "${var.name}-vpc-flow-log [ACCEPT]",
        "query" : "SOURCE '${local.flow_log_cloudwatch_log_group_name}' | fields @timestamp, @message | filter @message like /ACCEPT/ | sort @timestamp desc | limit 50",
        "view" : "table"
      }
    },
    {
      "type" : "log",
      "x" : 0,
      "y" : 12,
      "width" : 15,
      "height" : 10,
      "properties" : {
        "region" : data.aws_region.current.name,
        "title" : "${var.name}-vpc-flow-log [REJECT]",
        "query" : "SOURCE '${local.flow_log_cloudwatch_log_group_name}' | fields @timestamp, @message | filter @message like /REJECT/ | sort @timestamp desc | limit 50",
        "view" : "table"
      }
    },
    {
      "type" : "log",
      "x" : 16,
      "y" : 0,
      "width" : 9,
      "height" : 10,
      "properties" : {
        "region" : data.aws_region.current.name,
        "title" : "${var.name}-vpc-flow-log [BYTE TRANSFER]",
        "query" : "SOURCE '${local.flow_log_cloudwatch_log_group_name}' | stats avg(bytes) as AvgBytes, min(bytes) as MinBytes, max(bytes) as MaxBytes by srcAddr, dstAddr | sort MaxBytes desc",
        "view" : "table"
      }
    },
    {
      "type" : "log",
      "x" : 16,
      "y" : 12,
      "width" : 9,
      "height" : 10,
      "properties" : {
        "region" : data.aws_region.current.name,
        "title" : "${var.name}-vpc-flow-log [TOP REJECTED]",
        "query" : "SOURCE '${local.flow_log_cloudwatch_log_group_name}' | filter action='REJECT' | stats count(*) as numRejections by srcAddr | sort numRejections desc | limit 20",
        "view" : "table"
      }
    },
  ]
  flow_log_dashboard_body = {
    widgets        = flatten(local.flow_log_dashboard_widgets)
    end            = null
    start          = null
    periodOverride = null
  }
  flow_log_clean_dashboard_body = { for k, v in local.flow_log_dashboard_body : k => v if v != null }
}

resource "aws_cloudwatch_dashboard" "flow_log_cloudwatch_dashboard" {
  count = local.create_flow_log_cloudwatch_log_group ? 1 : 0

  dashboard_name = "${var.name}-flow-log"
  dashboard_body = jsonencode(local.flow_log_clean_dashboard_body)

  lifecycle { ignore_changes = [dashboard_body] }
}

############################################################
# Flow Log CloudWatch Cross-account Sharing
############################################################

resource "aws_iam_role" "flow_log_cross_account_role" {
  count = length(var.flow_log_cloudWatch_share_account_ids) > 0 ? 1 : 0

  name                 = "CloudWatch-CrossAccountSharingRole"
  path                 = "/"
  max_session_duration = 3600
  description          = "CloudWatch-CrossAccountSharingRole"
  assume_role_policy   = data.aws_iam_policy_document.flow_log_cross_account_assume_role.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess",
    "arn:aws:iam::aws:policy/AWSXrayReadOnlyAccess",
    "arn:aws:iam::aws:policy/CloudWatchReadOnlyAccess",
    "arn:aws:iam::aws:policy/CloudWatchAutomaticDashboardsAccess"
  ]
}

data "aws_iam_policy_document" "flow_log_cross_account_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = length(var.flow_log_cloudWatch_share_account_ids) > 0 ? [for i in var.flow_log_cloudWatch_share_account_ids : "arn:aws:iam::${i}:root"] : []
    }
  }
}

############################################################
# TGW Attachment
############################################################

resource "aws_ec2_transit_gateway_vpc_attachment" "this" {
  count = var.create_vpc && var.tgw_create_attachment && length(var.tgw_id) > 0 && var.tgw_create_attachment && length(var.tgw_subnets) > 0 ? 1 : 0

  vpc_id                 = local.vpc_id
  transit_gateway_id     = var.tgw_id
  subnet_ids             = aws_subnet.tgw[*].id
  dns_support            = var.enable_dns_support ? "enable" : "disable"
  ipv6_support           = var.enable_ipv6 && length(var.tgw_subnet_ipv6_prefixes) > 0 ? "enable" : "disable"
  appliance_mode_support = var.tgw_enable_appliance_mode ? "enable" : "disable"

  transit_gateway_default_route_table_association = var.tgw_default_rt_association
  transit_gateway_default_route_table_propagation = var.tgw_default_rt_propagation

  tags = merge(
    {
      "Name" = "${var.name}-tgw-attachment"
    },
    var.tags,
    var.tgw_attachment_tags,
  )

  depends_on = [aws_subnet.tgw]
}

############################################################
# Defaults
############################################################

resource "aws_default_vpc" "this" {
  count = var.manage_default_vpc ? 1 : 0

  enable_dns_support   = var.default_vpc_enable_dns_support
  enable_dns_hostnames = var.default_vpc_enable_dns_hostnames

  tags = merge(
    { "Name" = coalesce(var.default_vpc_name, "default") },
    var.tags,
    var.default_vpc_tags,
  )
}
