############################################################
# Switches
############################################################

variable "create_vpc" {
  type    = bool
  default = true
}

variable "create_igw" {
  type    = bool
  default = true
}

variable "create_egress_only_igw" {
  type    = bool
  default = true
}

variable "enable_nat_gateway" {
  type    = bool
  default = false
}

variable "enable_customer_nat" {
  type    = bool
  default = false
}

variable "enable_flow_log" {
  type    = bool
  default = false
}

variable "tgw_create_attachment" {
  type    = bool
  default = false
}

variable "enable_firewall_routing" {
  type    = bool
  default = false
}

############################################################
# VPC
############################################################

variable "name" {
  type    = string
  default = ""
}

variable "cidr" {
  type    = string
  default = "0.0.0.0/0"
}

variable "enable_ipv6" {
  type    = bool
  default = false
}

variable "private_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "database_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "public_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "intra_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "vpn_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "customer_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "tgw_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "firewall_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "endpoint_subnet_ipv6_prefixes" {
  type    = list(string)
  default = []
}

variable "assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "private_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "database_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "public_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "intra_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "vpn_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "customer_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "tgw_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "firewall_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "endpoint_subnet_assign_ipv6_address_on_creation" {
  type    = bool
  default = false
}

variable "secondary_cidrs" {
  type    = list(string)
  default = []
}

variable "instance_tenancy" {
  type    = string
  default = "default"
}

variable "public_subnet_suffix" {
  type    = string
  default = "public"
}

variable "private_subnet_suffix" {
  type    = string
  default = "private"
}

variable "database_subnet_suffix" {
  type    = string
  default = "database"
}

variable "intra_subnet_suffix" {
  type    = string
  default = "intra"
}

variable "vpn_subnet_suffix" {
  type    = string
  default = "vpn"
}

variable "customer_subnet_suffix" {
  type    = string
  default = "customer"
}

variable "tgw_subnet_suffix" {
  type    = string
  default = "tgw"
}

variable "firewall_subnet_suffix" {
  type    = string
  default = "firewall"
}

variable "endpoint_subnet_suffix" {
  type    = string
  default = "endpoint"
}

variable "public_subnets" {
  type    = list(string)
  default = []
}

variable "private_subnets" {
  type    = list(string)
  default = []
}

variable "database_subnets" {
  type    = list(string)
  default = []
}

variable "intra_subnets" {
  type    = list(string)
  default = []
}

variable "vpn_subnets" {
  type    = list(string)
  default = []
}

variable "customer_subnets" {
  type    = list(string)
  default = []
}

variable "customer_private_routes" {
  type    = list(string)
  default = []
}

variable "customer_database_routes" {
  type    = list(string)
  default = []
}

variable "tgw_subnets" {
  type    = list(string)
  default = []
}

variable "firewall_subnets" {
  type    = list(string)
  default = []
}

variable "endpoint_subnets" {
  type    = list(string)
  default = []
}

variable "azs" {
  type    = list(string)
  default = []
}

variable "enable_dns_hostnames" {
  type    = bool
  default = true
}

variable "enable_dns_support" {
  type    = bool
  default = true
}

variable "nat_gateway_destination_cidr_block" {
  type    = string
  default = "0.0.0.0/0"
}

variable "single_nat_gateway" {
  type    = bool
  default = true
}

variable "one_nat_gateway_per_az" {
  type    = bool
  default = false
}

variable "reuse_nat_ips" {
  type    = bool
  default = false
}

variable "external_nat_ip_ids" {
  type    = list(string)
  default = []
}

variable "external_nat_ips" {
  type    = list(string)
  default = []
}

variable "map_public_ip_on_launch" {
  type    = bool
  default = true
}

variable "manage_default_route_table" {
  type    = bool
  default = true
}

variable "default_route_table_name" {
  type    = string
  default = null
}

variable "default_route_table_propagating_vgws" {
  type    = list(string)
  default = []
}

variable "default_route_table_routes" {
  type    = list(map(string))
  default = []
}

variable "enable_dhcp_options" {
  type    = bool
  default = false
}

variable "dhcp_options_domain_name" {
  type    = string
  default = ""
}

variable "dhcp_options_domain_name_servers" {
  type    = list(string)
  default = ["AmazonProvidedDNS"]
}

variable "dhcp_options_ntp_servers" {
  type    = list(string)
  default = []
}

variable "dhcp_options_netbios_name_servers" {
  type    = list(string)
  default = []
}

variable "dhcp_options_netbios_node_type" {
  type    = string
  default = ""
}

variable "manage_default_vpc" {
  type    = bool
  default = false
}

variable "default_vpc_name" {
  type    = string
  default = null
}

variable "default_vpc_enable_dns_support" {
  type    = bool
  default = true
}

variable "default_vpc_enable_dns_hostnames" {
  type    = bool
  default = false
}

variable "default_vpc_enable_classiclink" {
  type    = bool
  default = false
}

variable "manage_default_network_acl" {
  type    = bool
  default = true
}

variable "default_network_acl_name" {
  type    = string
  default = null
}

variable "public_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "private_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "database_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "intra_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "vpn_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "customer_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "tgw_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "firewall_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "endpoint_dedicated_network_acl" {
  type    = bool
  default = true
}

variable "default_network_acl_ingress" {
  type = list(map(string))

  default = [
    {
      rule_no    = 100
      action     = "allow"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
      cidr_block = "0.0.0.0/0"
    },
    {
      rule_no         = 101
      action          = "allow"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      ipv6_cidr_block = "::/0"
    },
  ]
}

variable "default_network_acl_egress" {
  type = list(map(string))

  default = [
    {
      rule_no    = 100
      action     = "allow"
      from_port  = 0
      to_port    = 0
      protocol   = "-1"
      cidr_block = "0.0.0.0/0"
    },
    {
      rule_no         = 101
      action          = "allow"
      from_port       = 0
      to_port         = 0
      protocol        = "-1"
      ipv6_cidr_block = "::/0"
    },
  ]
}

variable "public_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "public_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "private_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "private_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "database_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "database_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "intra_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "intra_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "vpn_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "vpn_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "customer_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "customer_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "tgw_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "tgw_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "firewall_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "firewall_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "endpoint_inbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "endpoint_outbound_acl_rules" {
  type = list(map(string))

  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "manage_default_security_group" {
  type    = bool
  default = true
}

variable "default_security_group_name" {
  type    = string
  default = null
}

variable "default_security_group_ingress" {
  type    = list(map(string))
  default = []
}

variable "default_security_group_egress" {
  type    = list(map(string))
  default = []
}

############################################################
# VPC Flow Log
############################################################

variable "vpc_flow_log_permissions_boundary" {
  type    = string
  default = null
}

variable "create_flow_log_cloudwatch_log_group" {
  type    = bool
  default = false
}

variable "create_flow_log_cloudwatch_iam_role" {
  type    = bool
  default = false
}

variable "flow_log_traffic_type" {
  type    = string
  default = "ALL"
}

variable "flow_log_destination_type" {
  type    = string
  default = "cloud-watch-logs"
}

variable "flow_log_log_format" {
  type    = string
  default = null
}

variable "flow_log_destination_arn" {
  type    = string
  default = ""
}

variable "flow_log_cloudwatch_iam_role_arn" {
  type    = string
  default = ""
}

variable "flow_log_cloudwatch_log_group_name" {
  type    = string
  default = "/aws/vpc-flow-log"
}

variable "flow_log_cloudwatch_log_group_retention_in_days" {
  type    = number
  default = null
}

variable "flow_log_cloudwatch_log_group_kms_key_id" {
  type    = string
  default = null
}

variable "flow_log_max_aggregation_interval" {
  type    = number
  default = 600
}

variable "flow_log_file_format" {
  type    = string
  default = "plain-text"
  validation {
    condition = can(regex("^(plain-text|parquet)$",
    var.flow_log_file_format))
    error_message = "ERROR valid values: plain-text, parquet."
  }
}

variable "flow_log_hive_compatible_partitions" {
  type    = bool
  default = false
}

variable "flow_log_per_hour_partition" {
  type    = bool
  default = false
}

variable "flow_log_cloudWatch_share_account_ids" {
  type    = list(string)
  default = []
}

############################################################
# TGW
############################################################

variable "tgw_id" {
  type    = string
  default = ""
}

variable "tgw_enable_appliance_mode" {
  type    = bool
  default = false
}

variable "tgw_default_rt_association" {
  type    = bool
  default = true
}

variable "tgw_default_rt_propagation" {
  type    = bool
  default = true
}

variable "private_tgw_routes" {
  type    = list(string)
  default = []
}

variable "private_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "database_tgw_routes" {
  type    = list(string)
  default = []
}

variable "database_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "intra_tgw_routes" {
  type    = list(string)
  default = []
}

variable "intra_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "vpn_tgw_routes" {
  type    = list(string)
  default = []
}

variable "vpn_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "customer_tgw_routes" {
  type    = list(string)
  default = []
}

variable "customer_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "firewall_tgw_routes" {
  type    = list(string)
  default = []
}

variable "firewall_tgw_routes_ipv6" {
  type    = list(string)
  default = []
}

variable "tgw_routes" {
  type    = list(string)
  default = ["0.0.0.0/0"]
}

variable "tgw_routes_ipv6" {
  type    = list(string)
  default = ["::/0"]
}

variable "firewall_endpoint_ids" {
  type    = list(string)
  default = []
}

############################################################
# Tags
############################################################

variable "default_vpc_tags" {
  type    = map(string)
  default = {}
}

variable "default_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "default_network_acl_tags" {
  type    = map(string)
  default = {}
}

variable "default_security_group_tags" {
  type    = map(string)
  default = {}
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "vpc_tags" {
  type    = map(string)
  default = {}
}

variable "igw_tags" {
  type    = map(string)
  default = {}
}

variable "public_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "private_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "database_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "intra_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "vpn_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "customer_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "tgw_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "firewall_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "endpoint_subnet_tags" {
  type    = map(string)
  default = {}
}

variable "public_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "private_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "database_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "intra_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "vpn_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "customer_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "tgw_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "firewall_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "endpoint_route_table_tags" {
  type    = map(string)
  default = {}
}

variable "public_acl_tags" {
  type    = map(string)
  default = {}
}

variable "private_acl_tags" {
  type    = map(string)
  default = {}
}

variable "database_acl_tags" {
  type    = map(string)
  default = {}
}

variable "intra_acl_tags" {
  type    = map(string)
  default = {}
}

variable "vpn_acl_tags" {
  type    = map(string)
  default = {}
}

variable "customer_acl_tags" {
  type    = map(string)
  default = {}
}

variable "tgw_acl_tags" {
  type    = map(string)
  default = {}
}

variable "firewall_acl_tags" {
  type    = map(string)
  default = {}
}

variable "endpoint_acl_tags" {
  type    = map(string)
  default = {}
}

variable "dhcp_options_tags" {
  type    = map(string)
  default = {}
}

variable "nat_gateway_tags" {
  type    = map(string)
  default = {}
}

variable "customer_nat_gateway_tags" {
  type    = map(string)
  default = {}
}

variable "nat_eip_tags" {
  type    = map(string)
  default = {}
}

variable "vpc_flow_log_tags" {
  type    = map(string)
  default = {}
}

variable "tgw_attachment_tags" {
  type    = map(string)
  default = {}
}
