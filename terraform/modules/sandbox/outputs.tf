output "resource_details" {
  description = "Basic details of EC2 and EFS resources."
  value       = local.resource_details
}
output "a_records" {
  description = "Hostname and IP of EC2 and EFS resources. Used to create DNS records in active directory."
  value       = local.a_records
}
output "idm_client_install_script" {
  description = "Script to install IDM client on EC2 instance."
  value       = local.idm_client_install_script
}
# Remove this once explained in docs
# Comment/uncomment as needed for troubleshooting
#output "user_data_templates" {
#  description = "User data templates for EC2 instances."
#  value       = local.user_data_templates
#}