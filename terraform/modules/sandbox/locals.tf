locals {
  account_id           = data.aws_caller_identity.current.account_id
  domain_controller_ip = "10.0.0.40"
  # domain_password = nonsensitive(
  #   rsadecrypt(aws_instance.main["win-dc"].password_data, tls_private_key.main.private_key_pem)
  # )
  resource_details = concat(
    [for each in aws_instance.main : {
      name       = each.tags["Name"]
      private_ip = each.private_ip
      id         = each.id
    }],
    # [for each in var.elastic_file_systems : {
    #   name       = each.name
    #   private_ip = aws_efs_mount_target.main[each.name].ip_address
    #   id         = aws_efs_mount_target.main[each.name].file_system_id
    # }]
  )
  a_records = jsonencode(
    concat(
      [for each in var.ec2_instances : {
        name       = each.name
        private_ip = each.private_ip
      }],
      ["placeholder"]
      # [for each in var.elastic_file_systems : {
      #   name       = each.name
      #   private_ip = each.private_ip
      # }]
    )
  )
  idm_client_install_script = "sudo ipa-client-install --principal admin@${upper(var.namespace)}.LOCAL --password 'password' --domain ${var.namespace}.local --server idm-dc00.${var.namespace}.local --unattended"
  # Nested for loop
  # First loops through each object in var.ec2_instances
  # Then loops through each file defined in user_data_templates
  # Expands each template file and joins them together with a newline
  user_data_templates = {
    for each in var.ec2_instances : each.name => join("\n", [
      for tpl in each.user_data_templates : templatefile(
        "${path.module}/templates/userdata/${tpl}", {
          hostname    = each.name
          domain_name = "${var.namespace}.local"
          vpc_cidr    = "10.0.0.0/24"
          a_records   = local.a_records
          #secret_name   = aws_secretsmanager_secret.main.name
          sns_topic_arn = aws_sns_topic.main.arn
        }
      )
    ])
  }
}