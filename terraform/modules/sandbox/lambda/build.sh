#!/bin/bash
# Set working directory for the Go Lambda function
cd /workspaces/ctlz/terraform/modules/sandbox/lambda
# Remove existing go.mod and go.sum files if they exist
rm -f go.mod go.sum
# Initialize Go module
go mod init sndbx84
# Install packages
./install.sh
echo "go mod tidy"
go mod tidy
# Build the Go executable for AWS Lambda
echo "go build"
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -tags lambda.norpc -o bootstrap main.go
# Zip the executable
echo "zip bootstrap.zip bootstrap"
zip bootstrap.zip bootstrap
# Change to the target directory
cd /workspaces/ctlz/terraform/stacks/sandbox
