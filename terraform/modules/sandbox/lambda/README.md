#https://docs.aws.amazon.com/lambda/latest/dg/foundation-arch.html
# https://docs.aws.amazon.com/lambda/latest/dg/golang-package.html
cd /workspaces/ctlz/terraform/modules/sandbox/lambda
go mod init sndbx84
go mod tidy
# go get github.com/aws/aws-lambda-go/lambda
bash -c "GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -tags lambda.norpc -o bootstrap main.go"
bash -c "zip bootstrap.zip bootstrap"
cd /workspaces/ctlz/terraform/stacks/sandbox
