- codebuild container w/ ansible ssm connect
- trigger lambda with:
    - ddb update streams
    - ssm parameter update eventbridge
    - imagebuilder build complete eventbridge

- imagebuilder build should post to ddb w/ amazon base image and w/ output ami

- ddb data will have timestamp, semver, base and child images at start of build.

- need to output amis to ssm parameter using special syntax
-https://github.com/awsdocs/aws-lambda-developer-guide/blob/main/sample-apps/blank-go/3-invoke.sh
