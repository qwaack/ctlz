variable "namespace" {
  description = "Identifier for AWS resources. Used to name and tag resources."
  type        = string
}
variable "aws_region" {
  description = "AWS region to deploy resources."
  type        = string
}
variable "sns_email_subscribers" {
  description = "List of email addresses to subscribe to the SNS topic."
  type        = set(string)
}
variable "ec2_instances" {
  description = "List of EC2 instances to create"
  type = list(object({
    name                    = string
    ami                     = string
    instance_type           = string
    get_password_data       = bool
    private_ip              = optional(string, null)
    user_data_templates     = optional(list(string), null)
    user_data_is_powershell = optional(bool, null)
  }))
}
# variable "elastic_file_systems" {
#   description = "List of elastic file systems to create"
#   type = list(object({
#     name       = string
#     private_ip = optional(string, null)
#   }))
# }