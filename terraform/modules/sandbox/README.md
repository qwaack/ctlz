# About

Bootstraps resources for testing and lab purposes. Ignores best practices for convenience. Not for production use.

## User Data

Instances are configured via user data scripts.

### Actions Taken

#### Windows

- Updates PowerShell package provider and trusts PSGallery
- Updates PSReadline
- Disables Windows Firewall
- Installs Windows Features
  - Active Directory for domain controller
  - RSAT for domain clients
- Updates hostname
- Configures Active Directory
  - Domain Controller
    - Installs new domain
    - Creates DNS records for linux/IdM resources on domain controller
  - Clients
    - Joins clients to domain
    - Creates domain administrator named `ssm-user` so that session manager will still work on domain controller

#### Linux

- Installs common packages
- Installs SSM agent
- Installs IdM tools
  - Server tools for idm server
  - client tools for idm clients
- Updates hostname
- Configures IdM
  - Installs IdM server without integrated dns

### Reviewing Execution Logs

#### Windows

```powershell
# A directory is created for each user data execution.
# Get the path of the most recent execution:
$Path = 'C:\Windows\system32\config\systemprofile\AppData\Local\Temp\'
$MostRecent = (Get-Childitem $Path |
    Sort-Object -Descending |
    Select-Object -First 1).FullName
# Get errors
Get-Content "$MostRecent\err.tmp"
# Get output
Get-Content "$MostRecent\output.tmp"
# Get script
Get-Content "$MostRecent\UserScript.ps1"
```

#### Linux

`sudo cat /var/log/cloud-init-output.log`

## Red Hat Identity Management (IdM)

[See docs](https://access.redhat.com/articles/1586893)

### IPA CLI examples

```shell
# Create dev users and group
ipa group-add dev
ipa user-add dev01 --first dev --last 01
ipa user-add dev02 --first dev --last 02
ipa group-add-member dev --users dev01 --users dev02

# Create dba users and group
ipa group-add dba
ipa user-add dba01 --first dba --last 01
ipa user-add dba02 --first dba --last 02
ipa group-add-member dba --users dba01 --users dba02

# Create admins HBAC rule
ipa hbacrule-add admins --servicecat all --hostcat all
ipa hbacrule-add-user admins --groups admins

# Create dev HBAC rule
ipa hbacrule-add dev --servicecat=all
ipa hbacrule-add-host dev --hosts idm-client-00.idm.local
ipa hbacrule-add-user dev --groups dev

# Create dba HBAC rule
ipa hbacrule-add dba --servicecat=all
ipa hbacrule-add-host dba --hosts idm-client-01.idm.local
ipa hbacrule-add-user dba --groups dba

# Disable default allow_all hbac rule
ipa hbacrule-disable allow_all
```

## User Documentation

### Update a file with vim

- `sudo vim pathtofile`
- `i` key to enter insert mode
- Make desired changes
- `esc` key to exit insert mode
- `:` to enter command mode
- `x` and then `enter` to save and exit

### Add IdM client/host

Connect to instance and run below command. If you've just launched the environment and the command fails wait 5-10 minutes and try again.

```shell
sudo ipa-client-install --principal admin@MYDOMAIN.LOCAL --password 'mypassword' --domain mydomain.local --server idm-dc00.mydomain.local --unattended
```

## To Do

- User data template updates
  - I want to incorporate timeout functionality, decrease number of repetitive messages and improve clarity of messages
    - May just want to update a static site as tasks are completed
- Add resource tags
- Document/test module examples
  - Should be able to leave efs and/or ec2 variables empty and only deploy network if desired
- Align IdM deployment with RedHat Documentation
  - Currently IdM domain name is same as the active directory domain. It should be different, can be a subdomain of active directory domain if desired
  - IdM without integrated DNS
    - IdM domain needs it's own zone in Active Directory DNS. Programmatically update this zone with the dns file provided by IdM.
  - IdM with integrated DNS
    - Haven't worked with this deployment type before, need to research reccomended way of integrating it with Active Directory
- I want to add profile functions as part of user data
  - e.g. function to output user data log
- I want to review the windows user data scripts and speed them up if possible
