/*
# main.tf

This terraform configuration ignores a number of best practices. It is intended to be a quick and dirty way to deploy a lab environment for testing and learning. It is not intended to be used in a production environment.

## VPC/Subnet Notes

VPC CIDR         - 10.0.0.0/24
Public Subnet 1  - 10.0.0.0/27   (10.0.0.0   - 10.0.0.31)
Private Subnet 1 - 10.0.0.32/27  (10.0.0.32  - 10.0.0.63)
Available        - 10.0.0.64/27  (10.0.0.64  - 10.0.0.95)
Available        - 10.0.0.96/27  (10.0.0.96  - 10.0.0.127)
Available        - 10.0.0.128/27 (10.0.0.128 - 10.0.0.159)
Available        - 10.0.0.160/27 (10.0.0.160 - 10.0.0.191)
Available        - 10.0.0.192/27 (10.0.0.192 - 10.0.0.223)
Available        - 10.0.0.224/27 (10.0.0.224 - 10.0.0.255)
*/

data "aws_caller_identity" "current" {}
output "account_id" {
  value = data.aws_caller_identity.current.account_id
}
output "caller_arn" {
  value = data.aws_caller_identity.current.arn
}
output "caller_user" {
  value = data.aws_caller_identity.current.user_id
}


# VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/24"
  tags = {
    Name = "${var.namespace}-vpc"
  }
}
# Domain controller needs to be first DNS server for Windows instances to join domain
resource "aws_vpc_dhcp_options" "main" {
  domain_name          = "${var.namespace}.local"
  domain_name_servers  = [local.domain_controller_ip, "AmazonProvidedDNS"]
  netbios_name_servers = [local.domain_controller_ip]
  netbios_node_type    = 2
}
resource "aws_vpc_dhcp_options_association" "main" {
  vpc_id          = aws_vpc.main.id
  dhcp_options_id = aws_vpc_dhcp_options.main.id
}
resource "aws_subnet" "pub_use1_az1" {
  cidr_block        = "10.0.0.0/27"
  vpc_id            = aws_vpc.main.id
  availability_zone = "us-east-1a"
  tags = {
    Name = "${var.namespace}-pub-use1-az1"
  }
}
resource "aws_subnet" "prv_use1_az1" {
  cidr_block        = "10.0.0.32/27"
  vpc_id            = aws_vpc.main.id
  availability_zone = "us-east-1a"
  tags = {
    Name = "${var.namespace}-prv-use1-az1"
  }
}
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.namespace}-igw"
  }
}
resource "aws_eip" "main" {
  domain = "vpc"
}
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.pub_use1_az1.id
  #private_ip = "10.0.0.10"
  tags = {
    Name = "${var.namespace}-ngw"
  }
}
resource "aws_route_table" "prv" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }
  tags = {
    Name = "${var.namespace}-rt-prv"
  }
}
resource "aws_route_table" "pub" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
  tags = {
    Name = "${var.namespace}-rt-pub"
  }
}
resource "aws_route_table_association" "pub" {
  subnet_id      = aws_subnet.pub_use1_az1.id
  route_table_id = aws_route_table.pub.id
}
resource "aws_route_table_association" "prv" {
  subnet_id      = aws_subnet.prv_use1_az1.id
  route_table_id = aws_route_table.prv.id
}
resource "aws_network_acl" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "main"
  }
}
resource "aws_security_group" "main" {
  name        = "${var.namespace}-sg"
  description = "Allow traffic from 10.0.0.0/24 CIDR"
  vpc_id      = aws_vpc.main.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/24"]
  }
  tags = {
    Name = "${var.namespace}-sg"
  }
}


# IAM
resource "aws_iam_instance_profile" "main" {
  name = "DefaultInstanceProfile"
  role = aws_iam_role.main.name
}
resource "aws_iam_role" "main" {
  name = var.namespace
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = ["ec2.amazonaws.com", "lambda.amazonaws.com"]
        }
      }
    ]
  })
}
resource "aws_iam_role_policy_attachment" "main" {
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
  #policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role = aws_iam_role.main.name
}
resource "aws_iam_role_policy" "inline" {
  name = "instance-inline-policy"
  role = aws_iam_role.main.id
  policy = templatefile("${path.module}/templates/iam/instance-inline-policy.json", {
    aws_region     = var.aws_region
    account_id     = local.account_id
    secret_name    = "${var.namespace}-local"
    sns_topic_name = "${var.namespace}-local"
  })
}


# Resources from tls provider and local provider
resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "aws_key_pair" "main" {
  key_name   = "${var.namespace}-win-key-pair"
  public_key = tls_private_key.main.public_key_openssh
}
resource "local_file" "main" {
  filename = "${aws_key_pair.main.key_name}.pem"
  content  = tls_private_key.main.private_key_pem
}


resource "aws_instance" "main" {
  for_each      = { for each in var.ec2_instances : each.name => each }
  ami           = each.value.ami
  instance_type = each.value.instance_type
  private_ip    = each.value.private_ip
  subnet_id     = aws_subnet.prv_use1_az1.id
  #key_name               = aws_key_pair.main.key_name
  iam_instance_profile   = "DefaultInstanceProfile"
  vpc_security_group_ids = [aws_security_group.main.id]
  get_password_data      = each.value.get_password_data
  # https://developer.hashicorp.com/terraform/language/expressions/strings#directives
  user_data_base64 = base64encode(<<EOT
%{if each.value.user_data_is_powershell == true~}
<powershell>
%{endif~}
${local.user_data_templates[each.value.name]}
%{if each.value.user_data_is_powershell == true~}
</powershell>
<persist>true</persist>
%{endif~}
EOT
  )
  tags = {
    Name = each.value.name
  }
  # aws_route_table_association.prv isn't created until aws_nat_gateway.main is created
  # instances depend on these resources for outbound internet access
  # user data scripts may fail if these aren't in place first
  depends_on = [
    aws_route_table_association.prv
  ]
}


# EFS
# resource "aws_efs_file_system" "main" {
#   for_each       = { for each in var.elastic_file_systems : each.name => each }
#   creation_token = each.value.name
#   tags = {
#     Name = each.value.name
#   }
# }
# resource "aws_efs_mount_target" "main" {
#   for_each        = { for each in var.elastic_file_systems : each.name => each }
#   file_system_id  = aws_efs_file_system.main[each.value.name].id
#   subnet_id       = aws_subnet.prv_use1_az1.id
#   ip_address      = each.value.private_ip
#   security_groups = [aws_security_group.main.id]
# }


# Secrets Manager
# resource "aws_secretsmanager_secret" "main" {
#   name = "${var.namespace}-local"
# }
# resource "aws_secretsmanager_secret_version" "main" {
#   secret_id     = aws_secretsmanager_secret.main.id
#   secret_string = local.domain_password
# }


# SNS
resource "aws_sns_topic" "main" {
  name = var.namespace
}
resource "aws_sns_topic_subscription" "main" {
  for_each  = var.sns_email_subscribers
  topic_arn = aws_sns_topic.main.arn
  protocol  = "email"
  endpoint  = each.key
}


# S3
resource "aws_s3_bucket" "main" {
  bucket        = "${var.namespace}-${local.account_id}"
  force_destroy = true
}
resource "aws_s3_bucket_server_side_encryption_configuration" "main" {
  bucket = aws_s3_bucket.main.id
  rule {
    bucket_key_enabled = false
    apply_server_side_encryption_by_default { sse_algorithm = "AES256" }
  }
}
resource "aws_s3_bucket_versioning" "main" {
  bucket = aws_s3_bucket.main.id
  versioning_configuration { status = "Enabled" }
}
resource "aws_s3_bucket_public_access_block" "main" {
  bucket                  = aws_s3_bucket.main.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_ownership_controls" "main" {
  bucket = aws_s3_bucket.main.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
resource "aws_s3_bucket_acl" "main" {
  bucket = aws_s3_bucket.main.id
  acl    = "private"
  depends_on = [
    aws_s3_bucket_ownership_controls.main
  ]
}
resource "aws_s3_bucket_policy" "main" {
  bucket = aws_s3_bucket.main.id
  policy = templatefile("${path.module}/templates/iam/bucket-policy.json", {
    account_id = local.account_id,
    bucket_arn = aws_s3_bucket.main.arn
  })
}

# DDB
resource "aws_dynamodb_table" "main" {
  name             = var.namespace
  table_class      = "STANDARD_INFREQUENT_ACCESS"
  stream_enabled   = true
  stream_view_type = "NEW_IMAGE"
  billing_mode     = "PAY_PER_REQUEST"
  hash_key         = "Timestamp"
  range_key        = "Version"
  attribute {
    name = "Timestamp"
    type = "S"
  }
  attribute {
    name = "Version"
    type = "S"
  }
}

# TODO:SSM LOGGING https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-logging.html

# Lambda
resource "aws_lambda_function" "this" {
  function_name = var.namespace
  handler       = "main"
  #runtime       = "provided.al2023"
  runtime   = "provided.al2"
  role      = aws_iam_role.main.arn
  s3_bucket = aws_s3_bucket.main.id
  s3_key    = "lambda/bootstrap.zip"
  #s3_object_version = aws_s3_object.lambda.version_id
  source_code_hash = filebase64sha256("${path.module}/lambda/bootstrap.zip")
}

resource "aws_lambda_function_event_invoke_config" "this" {
  function_name                = aws_lambda_function.this.function_name
  maximum_event_age_in_seconds = 10800 # 3 hours
  maximum_retry_attempts       = 2
}
# resource "aws_lambda_permission" "this" {
#   action        = "lambda:InvokeFunction"
#   function_name = aws_lambda_function.this.arn
#   principal     = "events.amazonaws.com"
#   source_arn    = aws_cloudwatch_event_rule.this.arn
# }
# resource "aws_lambda_event_source_mapping" "this" {
#   event_source_arn = aws_sqs_queue.this.arn
#   function_name    = aws_lambda_function.this.arn
# }


resource "aws_s3_object" "lambda" {
  bucket = aws_s3_bucket.main.id
  key    = "lambda/bootstrap.zip"
  source = "${path.module}/lambda/bootstrap.zip"
  etag   = filemd5("${path.module}/lambda/bootstrap.zip")
}

# Imagebuilder
resource "aws_imagebuilder_infrastructure_configuration" "main" {
  description                   = var.namespace
  instance_profile_name         = aws_iam_instance_profile.main.name
  instance_types                = ["t3.micro"]
  name                          = var.namespace
  security_group_ids            = [aws_security_group.main.id]
  sns_topic_arn                 = aws_sns_topic.main.arn
  subnet_id                     = aws_subnet.prv_use1_az1.id
  terminate_instance_on_failure = true
  logging {
    s3_logs {
      s3_bucket_name = aws_s3_bucket.main.bucket
      s3_key_prefix  = "imagebuilder"
    }
  }
}
# Retrieve the SSM Parameter
data "aws_ssm_parameter" "al2023_ami" {
  name = "/aws/service/ami-amazon-linux-latest/al2023-ami-minimal-kernel-6.1-x86_64"
}

# Use the AMI ID value in an AMI data source
data "aws_ami" "al2023" {
  most_recent = true
  filter {
    name   = "image-id"
    values = [data.aws_ssm_parameter.al2023_ami.value]
  }
  owners = ["self", "amazon"]
}
resource "aws_imagebuilder_image_recipe" "main" {
  block_device_mapping {
    device_name = "/dev/xvdb"
    ebs {
      delete_on_termination = true
      volume_size           = 10
      volume_type           = "gp2"
    }
  }
  component {
    component_arn = "arn:aws:imagebuilder:us-east-1:aws:component/hello-world-linux/1.0.0/1"
  }
  name         = var.namespace
  parent_image = data.aws_ssm_parameter.al2023_ami.value
  version      = "0.0.1"
}
# resource "aws_imagebuilder_image" "base" {
#   distribution_configuration_arn   = aws_imagebuilder_distribution_configuration.main.arn
#   image_recipe_arn                 = aws_imagebuilder_image_recipe.main.arn
#   infrastructure_configuration_arn = aws_imagebuilder_infrastructure_configuration.main.arn
#   workflow {
#     workflow_arn = aws_imagebuilder_workflow.build.arn
#   }
# }
resource "aws_imagebuilder_workflow" "build" {
  name    = "${var.namespace}-build"
  version = "1.0.0"
  type    = "BUILD"

  data = <<-EOT
  name: "${var.namespace}-build"
  description: "${var.namespace}-build"
  schemaVersion: 1.0

  steps:
    - name: LaunchBuildInstance
      action: LaunchInstance
      onFailure: Abort
      inputs:
        waitFor: "ssmAgent"

  #   - name: ApplyBuildComponents
  #     action: ExecuteComponents
  #     onFailure: Abort
  #     inputs:
  #       instanceId.$: "$.stepOutputs.LaunchBuildInstance.instanceId"

    - name: CreateOutputAMI
      action: CreateImage
      onFailure: Abort
      if:
        stringEquals: "AMI"
        value: "$.imagebuilder.imageType"
      inputs:
        instanceId.$: "$.stepOutputs.LaunchBuildInstance.instanceId"

    - name: TerminateBuildInstance
      action: TerminateInstance
      onFailure: Continue
      inputs:
        instanceId.$: "$.stepOutputs.LaunchBuildInstance.instanceId"

  outputs:
    - name: "ImageId"
      value: "$.stepOutputs.CreateOutputAMI.imageId"
  EOT
}