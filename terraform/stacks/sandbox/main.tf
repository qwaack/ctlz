terraform {
  required_version = "~> 1.6.4"
  backend "s3" {
    dynamodb_table = "sndbx84-state-ddb"
    bucket         = "sndbx84-state-bucket"
    key            = "sndbx84/terraform.tfstate"
    encrypt        = true
    region         = "us-east-1"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.55.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.4"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.0"
    }
  }
}
provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      org = "qwaack"
    }
  }
}

# Retrieve the SSM Parameter
data "aws_ssm_parameter" "al2023_ami" {
  name = "/aws/service/ami-amazon-linux-latest/al2023-ami-minimal-kernel-6.1-x86_64"
}

# Use the AMI ID value in an AMI data source
data "aws_ami" "al2023" {
  most_recent = true
  filter {
    name   = "image-id"
    values = [data.aws_ssm_parameter.al2023_ami.value]
  }

  owners = ["self", "amazon"]
}

module "sandbox" {
  source                = "../../modules/sandbox/"
  namespace             = "sndbx84"
  aws_region            = "us-east-1"
  sns_email_subscribers = ["qwaack@pm.me"]
  ec2_instances = [
    {
      name                = "al2023base"
      ami                 = data.aws_ssm_parameter.al2023_ami.value
      instance_type       = "t3.micro"
      get_password_data   = false
      private_ip          = "10.0.0.54"
      user_data_templates = ["al2023.tftpl"]
    }
  ]
}
#
#