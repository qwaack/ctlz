﻿
Install-PSResource -TrustRepository -Scope CurrentUser -RequiredResource  @{
    'AWS.Tools.CloudFormation'            = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.CodeCommit'                = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.CodePipeline'              = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.Common'                    = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.IdentityManagement'        = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.IdentityStore'             = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.Installer'                 = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.Organizations'             = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.S3'                        = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SecretsManager'            = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SecurityToken'             = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SimpleNotificationService' = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SimpleSystemsManagement'   = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SSO'                       = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SSOAdmin'                  = @{
        repository = 'PSGallery'
    }
    'AWS.Tools.SSOOIDC'                   = @{
        repository = 'PSGallery'
    }
    'AWSLambdaPSCore'                     = @{
        repository = 'PSGallery'
    }
}
