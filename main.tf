terraform {
  required_version = "~> 1.6.4"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.67.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      org = "qwaack"
    }
  }
}

variable "account_factory_product_id" {
  description = "Account factory product id"
  type        = string
  validation {
    condition     = length(var.account_factory_product_id) > 0
    error_message = "The account_factory_product_id must not be empty."
  }
  sensitive = false
}
variable "account_factory_product_version" {
  description = "Account factory product version id"
  type        = string
  validation {
    condition     = length(var.account_factory_product_version) > 0
    error_message = "The account_factory_product_version must not be empty."
  }
  sensitive = false
}
variable "accounts" {
  description = "Account factory accounts"
  type = list(object({
    name                     = string
    provisioning_artifact_id = string
    product_id               = string
    provisioning_parameters = list(object({
      key   = string
      value = string
    }))
  }))
  validation {
    condition     = length(var.accounts) > 0
    error_message = "The accounts list must not be empty."
  }
  sensitive = false
}
variable "groups" {
  description = "Define IAMIDC groups, group members and account names for permission set assignment."
  type = list(object({
    name              = string
    aws_account_names = list(string)
    users = list(object({
      email      = string
      first_name = string
      last_name  = string
    }))
  }))
  sensitive = false
}

data "aws_caller_identity" "current" {}
data "aws_organizations_organization" "main" {}
data "aws_organizations_organizational_units" "main" {
  parent_id = data.aws_organizations_organization.main.roots[0].id
}
data "aws_servicecatalog_product" "main" {
  id = var.account_factory_product_id
}
data "aws_ssoadmin_instances" "main" {}

locals {
  project_name = "project0"
  data = {
    account_id               = data.aws_caller_identity.current.account_id
    org                      = data.aws_organizations_organization.main
    org_units                = data.aws_organizations_organizational_units.main
    account_factory_product  = data.aws_servicecatalog_product.main
    identity_center_instance = data.aws_ssoadmin_instances.main
    unique_users = [
      for _, users in { for user in flatten([
        for group in var.groups : [
          for user in group.users : {
            email      = user.email,
            first_name = user.first_name,
            last_name  = user.last_name
          }
        ]
      ]) : user.email => user... } :
      users[length(users) - 1]
    ]
  }
  sandbox_ou_id = { for ou in data.aws_organizations_organizational_units.main.children : ou.name => ou.id }["sandbox"]
  group_memberships = merge([
    for group in var.groups : {
      for user in group.users : "${group.name}-${user.email}" => {
        group_name = group.name,
        user_email = user.email
      }
    }
  ]...)
  group_account_assignments = merge([
    for group in var.groups : {
      for account_name in group.aws_account_names : "${group.name}-${account_name}" => {
        group_name = group.name,
        # Lookup account id
        account_id = ([
          for account in data.aws_organizations_organization.main.accounts : account.id
          if account.name == account_name
        ][0])
      }
    }
  ]...)
}

output "data" {
  value = { for k, v in local.data : k => v }
}

resource "aws_servicecatalog_provisioned_product" "main" {
  for_each                 = { for acc in var.accounts : acc.name => acc }
  name                     = each.value.name
  provisioning_artifact_id = each.value.provisioning_artifact_id
  product_id               = each.value.product_id
  dynamic "provisioning_parameters" {
    for_each = each.value.provisioning_parameters
    content {
      key   = provisioning_parameters.value.key
      value = provisioning_parameters.value.value
    }
  }
}
# Create Users
# resource "aws_identitystore_user" "main" {
#   for_each          = { for user in local.data.unique_users : user.email => user }
#   identity_store_id = data.aws_ssoadmin_instances.main.identity_store_ids[0]
#   display_name      = "${each.value.first_name} ${each.value.last_name}"
#   user_name         = each.value.email
#   name {
#     given_name  = each.value.first_name
#     family_name = each.value.last_name
#   }
#   emails {
#     value = each.value.email
#   }
# }
# # Create Groups
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/identitystore_group
# resource "aws_identitystore_group" "main" {
#   for_each = {for group in var.groups : group.name => group}
#   display_name      = "${local.project_name}${each.value.name}"
#   description       = "${each.value.name} Group for ${local.project_name} project"
#   identity_store_id = data.aws_ssoadmin_instances.main.identity_store_ids[0]
# }
# # Add users to groups
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/identitystore_group_membership
# resource "aws_identitystore_group_membership" "main" {
#   for_each          = local.group_memberships
#   identity_store_id = data.aws_ssoadmin_instances.main.identity_store_ids[0]
#   group_id          = aws_identitystore_group.main[each.value.group_name].group_id
#   member_id         = aws_identitystore_user.main[each.value.user_email].user_id
# }
# # Create Permission Sets
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssoadmin_permission_set
# resource "aws_ssoadmin_permission_set" "main" {
#   for_each    = { for group in var.groups : group.name => group }
#   name         = "${local.project_name}${each.value.name}"
#   description  = "Permission set for group ${each.value.name} in project ${local.project_name} project"
#   instance_arn = data.aws_ssoadmin_instances.main.arns[0]
# }
# # Permission Set Inline Policy
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssoadmin_permission_set_inline_policy
# resource "aws_ssoadmin_permission_set_inline_policy" "main" {
#   for_each = { for group in var.groups : group.name => group }
#   inline_policy = templatefile(
#     "${path.root}/templates/permission-set-inline-policies/group/${each.value.name}.json", {}
#   )
#   instance_arn       = data.aws_ssoadmin_instances.main.arns[0]
#   permission_set_arn = aws_ssoadmin_permission_set.main[each.value.name].arn
# }
# # Group Permission Set Account Assignment
# # https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssoadmin_account_assignment
# resource "aws_ssoadmin_account_assignment" "main" {
#   for_each           = local.group_account_assignments
#   instance_arn       = data.aws_ssoadmin_instances.main.arns[0]
#   permission_set_arn = aws_ssoadmin_permission_set.main[each.value.group_name].arn
#   principal_id       = aws_identitystore_group.main[each.value.group_name].group_id
#   principal_type     = "GROUP"
#   target_id          = each.value.account_id
#   target_type        = "AWS_ACCOUNT"
# }

